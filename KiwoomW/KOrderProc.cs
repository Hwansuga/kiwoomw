﻿using AxKHOpenAPILib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KiwoomW
{
    class KOrderProc : ProcScript
    {
        public void PrintLog(string msg_)
        {
            if (Global.logViewer == null)
                return;

            Global.logViewer.Print_CommonLog(msg_);
        }

        public override void StartCurJob()
        {
            PrintLog("▶" + curJob.procName);
            int ret = int.Parse(curJob.DoProc().ToString());
            if (ret == 0)
            {
                if (curJob != null && curJob.endAction != null)
                    curJob.endAction();
                //work success
                //wait callback

            }
            else
            {
                //job work failed
                string err = ErrorOccur.GetErrString(ret) + "(" + ret + ")";
                PrintLog(curJob.procName + " err / " + err);

                if (curJob.failAction != null)
                    curJob.failAction();

                DoneProc();
            }         
        }

        public void OnReceiveTrData(object sender, _DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            MethodInfo func = this.GetType().GetMethod(e.sRQName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (func == null)
                return;

            object[] prams = new object[1];
            prams[0] = e;
            func.Invoke(this, prams);
        }

        public void Req_Login()
        {
            AddProc((param) => { KiwoomAPI.Get.CommConnect(); return 0; } , null , "로그인");
        }

        public void OnEventConnect(object sender, _DKHOpenAPIEvents_OnEventConnectEvent e)
        {
            //
            DoneProc();
        }

        public void Req_RealData(string screenName_, List<string> listCode_)
        {
            string strCodeList = "";
            for (int i = 0; i < listCode_.Count; ++i)
            {
                Global.dicPreStockPrice[listCode_[i]] = "0";
                Global.dicStockPrice[listCode_[i]] = "0";

                strCodeList += listCode_[i];
                if (i != (listCode_.Count - 1))
                    strCodeList += ';';
            }

            AddProc((param) => {
                return KiwoomAPI.Get.SetRealReg(Global.GetScreenNo(screenName_).ToString(), strCodeList, "10;992;13028;13029", "0");
            } , null  , "실시간 데이터 요청",
            () => { DoneProc(); });           
        }

        public void Req_FutureAccountInfo()
        {
            string procName = "선물 계정 조회";
            AddProc((param) => 
            {               
                DeleteProc(procName);
                KiwoomAPI.Get.SetInputValue("계좌번호", Global.comboBox_Account);
                KiwoomAPI.Get.SetInputValue("비밀번호", "");
                KiwoomAPI.Get.SetInputValue("비밀번호입력매체구분", "");
            
                string ans_Name = "Ans_FutureAccountInfo";
            
                return KiwoomAPI.Get.CommRqData(ans_Name, "OPW20010", 0, Global.GetScreenNo(ans_Name).ToString());
            
            } , null , procName);

            
        }

        public void Req_TotalProfit()
        {
            string procName = "실현손익 조회";
            AddProc((param) =>
            {
                DeleteProc(procName);
                KiwoomAPI.Get.SetInputValue("계좌번호", Global.comboBox_Account);

                string ans_Name = "Ans_TotalProfit";

                return KiwoomAPI.Get.CommRqData(ans_Name, "OPT50031", 0, Global.GetScreenNo(ans_Name).ToString());

            }, null, procName);
        }

        void Ans_FutureAccountInfo(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            DoneProc();
        }

        void Ans_TotalProfit(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            DoneProc();
        }

        public void Req_HavingFutureStockInfo()
        {
            string procName = "보유 현황 체크";
            AddProc((param) =>
            {              
                DeleteProc(procName);
                KiwoomAPI.Get.SetInputValue("계좌번호", Global.comboBox_Account.Trim());
                string ans_Name = "Ans_HavingFutureStockInfo";

                return KiwoomAPI.Get.CommRqData(ans_Name, "OPT50027", 0, Global.GetScreenNo(ans_Name).ToString());
            }, null, procName);
        }
        void Ans_HavingFutureStockInfo(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            DoneProc();
        }

        public void Req_OrderingInfo()
        {
            string procName = "주문 현황 체크";
            AddProc((param) =>
            {               
                DeleteProc(procName);
                KiwoomAPI.Get.SetInputValue("종목코드", "");
                KiwoomAPI.Get.SetInputValue("조회구분", "1");
                KiwoomAPI.Get.SetInputValue("매매구분", "0");
                KiwoomAPI.Get.SetInputValue("체결구분", "1");
                KiwoomAPI.Get.SetInputValue("계좌번호", Global.comboBox_Account.Trim());

                string ans_Name = "Ans_OrderingInfo";

                return KiwoomAPI.Get.CommRqData(ans_Name, "OPT50026", 0, Global.GetScreenNo(ans_Name).ToString());
            }, null, procName);
        }

        void Ans_OrderingInfo(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            DoneProc();
        }

        public void CleanAll()
        {
            
            PrintLog("청산 ");
            if (listProcData.Count > 0)
                PrintLog("=== 작업취소 리스트 ===");
            foreach (var item in listProcData)
                PrintLog(item.procName + " 작업 취소");
           
            DeleteAllProc();

            Req_HavingFutureStockInfo();
            Req_OrderingInfo();

            foreach (OrderInfo item in Global.listOrderIfo)
            {
                if (string.IsNullOrEmpty(item.종목코드))
                    continue;

                if (string.IsNullOrEmpty(item.주문수량))
                    continue;

                if (int.Parse(item.주문수량.Trim()) <= 0)
                    continue;

                PrintLog(item.주문가격 + " / " + item.주문수량 + " 주문 취소");

                AddProc((param) => {
                    string ans_Name = "Cancel_Order";
                    return KiwoomAPI.Get.SendOrderFO(ans_Name , Global.GetScreenNo(ans_Name).ToString() ,Global.comboBox_Account ,item.종목코드,3, "","",int.Parse(item.주문수량.Trim()),item.주문가격, item.주문번호);
                } , null , item.주문가격 + "/ " + item.주문수량 + " 주문 취소",
                ()=> { Thread.Sleep(300); });
            }

            foreach (StockInfo item in Global.listStockInfo)
            {
                if (string.IsNullOrEmpty(item.종목코드))
                    continue;

                if (string.IsNullOrEmpty(item.보유수량))
                    continue;

                if (int.Parse(item.보유수량.Trim()) <= 0)
                    continue;

                string purchaseType = item.매매구분;
                if (item.매매구분 == "1")
                    purchaseType = "2";
                else if (item.매매구분 == "2")
                    purchaseType = "1";

                PrintLog(item.매입단가 + " / " + item.보유수량 + " 처분");

                AddProc((param) =>
                {
                    string ans_Name = "Sell_Stock";
                    return KiwoomAPI.Get.SendOrderFO(ans_Name, Global.GetScreenNo(ans_Name).ToString(), Global.comboBox_Account, Global.comboBox_KospiCode, 1,purchaseType, "3",int.Parse(item.보유수량.Trim()), "", item.주문번호);
                }, null, item.매입단가 + "/ " + item.보유수량 + " 처분",
                () => { Thread.Sleep(300);});
            }
        }

        public void Req_Order(string ans_Name_ , string orderType_ , float price_)
        {
            AddProc((param) =>
            {
                return KiwoomAPI.Get.SendOrderFO(ans_Name_ , Global.GetScreenNo(ans_Name_).ToString() , Global.comboBox_Account, Global.comboBox_KospiCode , 1 , orderType_ ,"1", Global.numericUpDown_NumPerDigit, price_.ToString() , "");
            }, null, ans_Name_ + " / " + orderType_ + " / " + Global.numericUpDown_NumPerDigit + " / " + price_.ToString(),
            ()=> { });
        }

        public void OnReceiveChejanData(object sender, _DKHOpenAPIEvents_OnReceiveChejanDataEvent e)
        {
            if (e.sGubun == "0")
            {
                List<int> listFid = e.sFIdList.Split(';').ToList().ConvertAll(o => int.Parse(o));

                if (listFid.Contains(913))
                {
                    if (KiwoomAPI.Get.GetChejanData(913).Trim() == "접수")
                    {
                        DoneProc();
                    }
                }               
            }
        }
    }
}
