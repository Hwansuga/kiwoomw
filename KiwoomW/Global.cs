﻿using AxKHOpenAPILib;
using KiwoomW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

public class SectionInfo
{
    public bool enable { get; set; }
    public float min { get; set; }
    public float max { get; set; }
    public int state { get; set; }
}

public class FutureAccInfoItem
{
    public string name { get; set; }
    public string value { get; set; }
}

public class StockInfo
{
    public string 종목코드 { get; set; }
    public string 매매구분 { get; set; }
    public string 보유수량 { get; set; }
    public string 매입단가 { get; set; }
    public string 주문번호 { get; set; }
}

public class OrderInfo
{
    public string 종목코드 { get; set; }
    public string 주문구분 { get; set; }
    public string 주문수량 { get; set; }
    public string 주문번호 { get; set; }
    public string 주문가격 { get; set; }
}

class Global : Singleton<Global>
{
    public static bool stop = true;

    public static LogViewer logViewer = new LogViewer();
    public static StockViewer stockViewer = new StockViewer();

    public static List<FutureAccInfoItem> futureAccInfo = new List<FutureAccInfoItem>();
    public static List<SectionInfo> listSectionInfo = new List<SectionInfo>();
    public static List<StockInfo> listStockInfo = new List<StockInfo>();
    public static List<OrderInfo> listOrderIfo = new List<OrderInfo>();

    public const int CP_NOCLOSE_BUTTON = 0x200;
    public const float oneDigit = 0.05f;
    public const long valuePerDigit = 12500;

    public static string comboBox_Account;
    public static string comboBox_KospiCode;

    public static int numericUpDown_MinPriceSection;
    public static int numericUpDown_MaxPriceSection;
    public static int numericUpDown_Level_SellRight;
    public static int numericUpDown_EnterValue;
    public static int numericUpDown_SellRight_Mount;
    public static int numericUpDown_NumPerDigit;
    public static int numericUpDown_Term_BuyRight;
    public static int numericUpDown_remainSec;
    public static int numericUpDown_CleanValue;

    public static Dictionary<string, int> screenNoDic = new Dictionary<string, int>();
    public static Dictionary<string, string> dicPreStockPrice = new Dictionary<string, string>();
    public static Dictionary<string, string> dicStockPrice = new Dictionary<string, string>();

    public static int GetScreenNo(string screenName_)
    {
        if (screenNoDic.ContainsKey(screenName_) == false)
        {
            screenNoDic.Add(screenName_, 5000 + screenNoDic.Count);
        }

        return screenNoDic[screenName_];
    }

    public static float GetStockPrice()
    {
        if (dicStockPrice.Count <= 0)
            return 0.0f;

        if (dicStockPrice.ContainsKey(comboBox_KospiCode) == false)
            return 0.0f;

        try
        {
            return float.Parse(dicStockPrice[comboBox_KospiCode].Replace("-", ""));
        }
        catch
        {
            return 0.0f;
        }
    }

    public static float GetPreStockPrice()
    {
        if (dicPreStockPrice.Count <= 0)
            return 0.0f;

        if (dicPreStockPrice.ContainsKey(comboBox_KospiCode) == false)
            return 0.0f;

        try
        {
            return float.Parse(dicPreStockPrice[comboBox_KospiCode].Replace("-", ""));
        }
        catch
        {
            return 0.0f;
        }
    }

    public static SectionInfo GetSectionInfo(float orderPrice_)
    {
        foreach (SectionInfo item in listSectionInfo)
        {
            if (item.min <= orderPrice_ && item.max > orderPrice_)
                return item;
        }

        return null;
    }
}

