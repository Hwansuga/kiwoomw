﻿using AxKHOpenAPILib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KiwoomW
{

    public partial class Form1 : Form
    {       
        KOrderProc kOrder = new KOrderProc();

        System.Windows.Forms.Timer timer1Sec = new System.Windows.Forms.Timer();

        void SaveSettingInfo()
        {
            List<string> listData = new List<string>();
            listData.Add(numericUpDown_MinPriceSection.Value.ToString());
            listData.Add(numericUpDown_MaxPriceSection.Value.ToString());
            listData.Add(numericUpDown_Level_SellRight.Value.ToString());
            listData.Add(numericUpDown_SellRight_Mount.Value.ToString());
            listData.Add(numericUpDown_NumPerDigit.Value.ToString());
            listData.Add(numericUpDown_CleanValue.Value.ToString());
            listData.Add(numericUpDown_Term_BuyRight.Value.ToString());
            listData.Add(numericUpDown_remainSec.Value.ToString());

            Util.SaveTxtFile("Setting.txt", listData);
        }

        void LoadSettingInfo()
        {
            List<string> listData = new List<string>();
            Util.LoadTxtFile("Setting.txt", ref listData);

            if (listData.Count <= 0)
                return;

            numericUpDown_MinPriceSection.Value = int.Parse(listData[0]);
            numericUpDown_MaxPriceSection.Value = int.Parse(listData[1]);
            numericUpDown_Level_SellRight.Value = int.Parse(listData[2]);
            numericUpDown_SellRight_Mount.Value = int.Parse(listData[3]);
            numericUpDown_NumPerDigit.Value = int.Parse(listData[4]);
            numericUpDown_CleanValue.Value = int.Parse(listData[5]);
            numericUpDown_Term_BuyRight.Value = int.Parse(listData[6]);
            if (listData.Count >= 10)
            {
                numericUpDown_remainSec.Value = int.Parse(listData[7]);
            }
        }

        public Form1()
        {
            InitializeComponent();

            FormClosing += ExitEvent;
        }

        void SetViwerPos()
        {
            Global.logViewer.Show();
            Util.SetFormPos(this, Global.logViewer);

            Global.stockViewer.Show();
            Util.SetFormPos(this, Global.stockViewer , FORMPOS.FROMPOS_RIGHT);
        }


        void ExitEvent(object sender, FormClosingEventArgs e)
        {
            foreach (KeyValuePair<string, int> item in Global.screenNoDic)
                KiwoomAPI.Get.DisconnectRealData(item.Value.ToString());

            KiwoomAPI.Get.SetRealRemove("ALL", "ALL");

            SaveSettingInfo();

            Application.ExitThread();
            Environment.Exit(0);
        }

        void InitCallbackEvent()
        {
            List<object> listReceiver = new List<object>();
            listReceiver.Add(Global.logViewer);
            listReceiver.Add(Global.stockViewer);
            listReceiver.Add(this);                                         
            listReceiver.Add(kOrder);

            CallbackEventUtil.AttachCallbackEvent(KiwoomAPI.Get, "OnEventConnect", listReceiver);
            CallbackEventUtil.AttachCallbackEvent(KiwoomAPI.Get, "OnReceiveConditionVer", listReceiver);
            CallbackEventUtil.AttachCallbackEvent(KiwoomAPI.Get, "OnReceiveTrCondition", listReceiver);
            CallbackEventUtil.AttachCallbackEvent(KiwoomAPI.Get, "OnReceiveRealCondition", listReceiver);
            CallbackEventUtil.AttachCallbackEvent(KiwoomAPI.Get, "OnReceiveTrData", listReceiver);
            CallbackEventUtil.AttachCallbackEvent(KiwoomAPI.Get, "OnReceiveRealData", listReceiver);
            CallbackEventUtil.AttachCallbackEvent(KiwoomAPI.Get, "OnReceiveChejanData", listReceiver);
            CallbackEventUtil.AttachCallbackEvent(KiwoomAPI.Get, "OnReceiveMsg", listReceiver);
        }

        void Make1SecEvent()
        {
            timer1Sec.Interval = 1000;
            timer1Sec.Tick += new EventHandler(CheckListProc);
            timer1Sec.Tick += new EventHandler(NowTime);
            timer1Sec.Start();
        }

        void CheckListProc(object sender, EventArgs e)
        {
            listBox_ListProc.Invoke(new MethodInvoker(delegate () {
                listBox_ListProc.Items.Clear();
                if (kOrder.curJob == null)
                    return;

                listBox_ListProc.Items.Add(kOrder.curJob.procName);
                foreach (var item in kOrder.listProcData)
                    listBox_ListProc.Items.Add(item.procName);
            }));
        }

        void NowTime(object sender, EventArgs e)
        {
            label_curTime.Invoke(new MethodInvoker(delegate () {
                label_curTime.Text = DateTime.Now.ToString("MM-dd HH:mm:ss");
            }));            
        }

        public void OnReceiveTrData(object sender, _DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            MethodInfo func = this.GetType().GetMethod(e.sRQName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (func == null)
                return;

            object[] prams = new object[1];
            prams[0] = e;
            func.Invoke(this, prams);
        }

        public void OnReceiveRealData(object sender, _DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            MethodInfo method = GetType().GetMethod(e.sRealType, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if (method != null)
                method.Invoke(this, new object[] { sender, e });
        }
        public void OnReceiveChejanData(object sender, _DKHOpenAPIEvents_OnReceiveChejanDataEvent e)
        {
            List<int> listFid = e.sFIdList.Split(';').ToList().ConvertAll(o => int.Parse(o));

            if (e.sGubun == "0")
            {                
                if (listFid.Contains(913))
                {
                    if (KiwoomAPI.Get.GetChejanData(913).Trim() == "체결")
                    {
                        Order_OtherSide(sender, e);
                        kOrder.Req_HavingFutureStockInfo();
                        kOrder.Req_OrderingInfo();
                        kOrder.Req_FutureAccountInfo();
                        kOrder.Req_TotalProfit();
                    }
                    else if (KiwoomAPI.Get.GetChejanData(913).Trim() == "접수")
                    {
                        kOrder.Req_OrderingInfo();
                    }
                }
            }
            else if (e.sGubun == "4")
            {
                kOrder.Req_FutureAccountInfo();
                kOrder.Req_TotalProfit();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EventUtil.ConnectCustomEvent(this);
            LoadSettingInfo();

            InitCallbackEvent();
            Make1SecEvent();
            SetViwerPos();

            ButtonInit();
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kOrder.Req_Login();         
        }

        void SetLoginInfo()
        {
            List<string> listAccount = KiwoomAPI.Get.GetLoginInfo("ACCLIST").Trim().Split(';').ToList();
            comboBox_Account.Items.Clear();
            foreach (string item in listAccount)
                comboBox_Account.Items.Add(item);

            if (comboBox_Account.Items.Count > 0)
            {
                comboBox_Account.SelectedIndex = 0;
            }

            label_UserName.Text = KiwoomAPI.Get.GetLoginInfo("USER_NAME");
            label_UserID.Text = KiwoomAPI.Get.GetLoginInfo("USER_ID");

            if (KiwoomAPI.Get.GetLoginInfo("GetServerGubun") == "1")
            {
                label_Server.Text = "모의서버";
            }
            else
            {
                label_Server.Text = "실제서버 " + KiwoomAPI.Get.GetLoginInfo("GetServerGubun");
            }

            loginToolStripMenuItem.Enabled = true;
            button_CheckAcc.Enabled = true;

            SetStockListInfo();
        }

        void SetStockListInfo()
        {
            List<string> listCode = KiwoomAPI.Get.GetFutureList().Split(';').ToList().FindAll(x => x.IndexOf("101") == 0);
            comboBox_KospiCode.Items.Clear();

            //한종목만 가져오게 설정
            listCode = listCode.GetRange(0, 1);

            foreach (string item in listCode)
            {
                comboBox_KospiCode.Items.Add(item);
            }

            if (comboBox_KospiCode.Items.Count > 0)
            {
                comboBox_KospiCode.SelectedIndex = 0;
            }

            kOrder.Req_RealData("선물시세", listCode);
        }

        void ParseSettingInfo()
        {
            Global.listSectionInfo.Clear();

            if (Global.numericUpDown_MinPriceSection <= 0)
                return;

            if (Global.numericUpDown_MaxPriceSection <= 0)
                return;

            float startPrice = Global.numericUpDown_MinPriceSection;

            float addValue = Global.numericUpDown_Level_SellRight * Global.oneDigit;
            while (true)
            {
                float endPrice = startPrice + addValue;
                if (endPrice > Global.numericUpDown_MaxPriceSection)
                    break;

                SectionInfo tmp = new SectionInfo();
                tmp.enable = true;
                tmp.min = startPrice;
                tmp.max = endPrice;
                tmp.state = 0;
                Global.listSectionInfo.Add(tmp);

                startPrice += addValue;
            }
            Global.listSectionInfo.Reverse();

            dataGridView_SectionInfo.Invoke(new MethodInvoker(delegate () {
                dataGridView_SectionInfo.DataSource = null;
                dataGridView_SectionInfo.DataSource = Global.listSectionInfo;
                dataGridView_SectionInfo.Refresh();
            }));

        }

        public void OnEventConnect(object sender, _DKHOpenAPIEvents_OnEventConnectEvent e)
        {
            if (e.nErrCode == 0)//if문으로 e.nErrCode가 0인지 검사해서 로그인이 잘 되었는지 체크합니다
            {
                SetLoginInfo();

                KiwoomAPI.Get.KOA_Functions("ShowAccountWindow", string.Empty);
            }
        }

        private void button_CheckAcc_Click(object sender, EventArgs e)
        {
            if (KiwoomAPI.Get.GetConnectState() == 0)
            {
                MessageBox.Show("로그인을 해 주세요");
                return;
            }
            button_Start.Enabled = true;

            kOrder.Req_FutureAccountInfo();
            kOrder.Req_TotalProfit();


            ParseSettingInfo();

            kOrder.Req_HavingFutureStockInfo();
            kOrder.Req_OrderingInfo();                    
        }

        void Ans_FutureAccountInfo(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {         
            List<string> listGetData = new List<string>();
            listGetData.Add("예탁총액");
            listGetData.Add("예탁현금");
            listGetData.Add("증거금총액");
            listGetData.Add("증거금현금");
            listGetData.Add("증거금대용금");
            listGetData.Add("주문가능총액");
            listGetData.Add("주문가능현금");
            listGetData.Add("주문가대용금");
            listGetData.Add("추가증거금총액");
            listGetData.Add("추가증거금현금");
            listGetData.Add("인출가능총액");
            listGetData.Add("인출가능현금");
            listGetData.Add("인출가능대용금");
            listGetData.Add("순자산금액");
            listGetData.Add("익일예탁총액");
            listGetData.Add("개장예탁총액");
            listGetData.Add("선물정산차금");
            listGetData.Add("선물정산손익");
            listGetData.Add("선물평가손익");
            listGetData.Add("선물약정금액");
            listGetData.Add("옵션결제차금");
            listGetData.Add("옵션청산손익");
            listGetData.Add("옵션평가손익");
            listGetData.Add("옵션약정금액");

            Global.futureAccInfo.Clear();
            foreach (string item in listGetData)
            {
                FutureAccInfoItem accInfoItem = new FutureAccInfoItem();

                accInfoItem.name = item;
                string data = KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, 0, item);
                if (string.IsNullOrEmpty(data))
                    continue;

                accInfoItem.value = long.Parse(data).ToString("n0");

                Global.futureAccInfo.Add(accInfoItem);
            }

            dataGridView_AccountInfo.Invoke(new MethodInvoker(delegate() {
                dataGridView_AccountInfo.DataSource = null;
                dataGridView_AccountInfo.DataSource = Global.futureAccInfo;
                dataGridView_AccountInfo.Refresh(); }));

            groupBox_SettingInfo.Enabled = true;

            this.Invoke(new MethodInvoker(delegate () {
                FutureAccInfoItem info = Global.futureAccInfo.Find(x => x.name == "선물정산차금");
                if (info != null)
                    label_winLoseValue2.Text = info.value;

                info = Global.futureAccInfo.Find(x => x.name == "순자산금액");
                if (info != null)
                    label_winLoseValue3.Text = info.value;
            }));
        }

        void Ans_TotalProfit(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            int cnt = KiwoomAPI.Get.GetRepeatCnt(e.sTrCode, e.sRQName);
            long totalValue = 0;
            for (int i = 0; i < cnt; ++i)
            {
                string data = KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, 0, "당일매도손익");
                if (string.IsNullOrEmpty(data))
                    continue;

                totalValue += long.Parse(data.Trim());
            }

            this.Invoke(new MethodInvoker(delegate ()
            {
                label_winLoseValue.Text = totalValue.ToString("N0");
            }));
        }

        void 선물시세(object sender, _DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            List<string> listCode = Global.dicStockPrice.Keys.ToList();
            foreach (string item in listCode)
            {
                Global.dicPreStockPrice[item] = Global.dicStockPrice[item];
                Global.dicStockPrice[item] = KiwoomAPI.Get.GetCommRealData(item, 10);
                
            }

            label_KospiPrice.Text = Global.dicStockPrice[Global.comboBox_KospiCode];

            if (Global.dicStockPrice[Global.comboBox_KospiCode].Contains('+'))
            {
                label_KospiPrice.ForeColor = Color.Red;
                label_KospiPrice.Text = label_KospiPrice.Text.Replace('+', '▲');
            }
            else if (Global.dicStockPrice[Global.comboBox_KospiCode].Contains('-'))
            {
                label_KospiPrice.ForeColor = Color.Blue;
                label_KospiPrice.Text = label_KospiPrice.Text.Replace('-', '▼');
            }

//             label_winLoseValue.Text = KiwoomAPI.Get.GetCommRealData(Global.comboBox_KospiCode, 992);
//             label_winLoseValue2.Text = KiwoomAPI.Get.GetCommRealData(Global.comboBox_KospiCode, 13028);
//             label_winLoseValue3.Text = KiwoomAPI.Get.GetCommRealData(Global.comboBox_KospiCode, 13029);

            //             //가격이 동일한경우 로직처리가 따로 필요없음
            //             if (Global.GetPreStockPrice() == Global.GetStockPrice())
            //                 return;
        }

        private void button_Allclear_Click(object sender, EventArgs e)
        {
            if (KiwoomAPI.Get.GetConnectState() == 0)
                return;
                   
            kOrder.CleanAll();

        }

        private void button_SetSetting_Click(object sender, EventArgs e)
        {
            if (Global.numericUpDown_MinPriceSection <= 0
                || Global.numericUpDown_MaxPriceSection <= 0
                || (Global.numericUpDown_MaxPriceSection <= Global.numericUpDown_MinPriceSection))
            {
                MessageBox.Show("상한선, 하한선 값을 확인해 주세요");
                return;
            }

            if (Global.numericUpDown_Level_SellRight <= 0)
            {
                MessageBox.Show("구간별 분할 단위(호) 값을 확인해 주세요");
                return;
            }

            foreach (SectionInfo item in Global.listSectionInfo)
            {
                item.state = 0;
            }

            dataGridView_SectionInfo.Invoke(new MethodInvoker(delegate ()
            {
                dataGridView_SectionInfo.DataSource = null;
                dataGridView_SectionInfo.DataSource = Global.listSectionInfo;
                dataGridView_SectionInfo.Refresh();
            }));
        }

        private void button_FixSection_Click(object sender, EventArgs e)
        {
            ParseSettingInfo();
        }

        SectionInfo GetSellRightSection()
        {
            float curPrice = Global.GetStockPrice();
            float preCurPrice = Global.GetPreStockPrice();

            List<SectionInfo> listEnable = Global.listSectionInfo.FindAll(x => x.state == 0);
            foreach (SectionInfo item in listEnable)
            {
                if ((item.min - Global.numericUpDown_EnterValue * Global.oneDigit) <= curPrice)
                {
                    if ((item.min - Global.numericUpDown_EnterValue * Global.oneDigit) > preCurPrice)
                    {
                        if ((item.max - Global.numericUpDown_CleanValue * Global.oneDigit) > curPrice)
                            return item;
                    }
                }
            }

            return null;
        }

        public void Order_OtherSide(object sender, _DKHOpenAPIEvents_OnReceiveChejanDataEvent e)
        {
            string completePrice = KiwoomAPI.Get.GetChejanData(910).Trim();
            string orderPrice = KiwoomAPI.Get.GetChejanData(901).Trim();
            string cnt = KiwoomAPI.Get.GetChejanData(911).Trim();
            string code = KiwoomAPI.Get.GetChejanData(9001).Trim();
            string screenNo = KiwoomAPI.Get.GetChejanData(920).Trim();

            kOrder.PrintLog(FIDManager.GetString(920) + " : " + screenNo +
                " / " + FIDManager.GetString(9001) + " : " + code +
                " / " + FIDManager.GetString(910) + " : " + completePrice +
                " / " + FIDManager.GetString(901) + " : " + orderPrice +
                " / " + FIDManager.GetString(911) + " : " + cnt);

            //청산 체결 일경우
            if (screenNo == Global.GetScreenNo("Sell_Stock").ToString()
                || screenNo == Global.GetScreenNo("반대편선물주문").ToString())
                return;

            if (string.IsNullOrEmpty(completePrice)
                || string.IsNullOrEmpty(cnt)
                || string.IsNullOrEmpty(code))
                return;


            float newPrice = float.Parse(orderPrice) - Global.numericUpDown_Term_BuyRight * Global.oneDigit;
            string typePuchase = "2";
            float cleanPrice = Global.GetSectionInfo(float.Parse(orderPrice)).max - Global.numericUpDown_CleanValue * Global.oneDigit;

            string purchaseType = KiwoomAPI.Get.GetChejanData(905).Trim();
            if (purchaseType.Contains("매도") == false)
            {
                newPrice = float.Parse(orderPrice) + Global.numericUpDown_Term_BuyRight * Global.oneDigit;
                typePuchase = "1";
                cleanPrice = Global.GetSectionInfo(float.Parse(orderPrice)).min + Global.numericUpDown_CleanValue * Global.oneDigit;
            }


            kOrder.PrintLog(typePuchase.ToString() + " 반대편 주문 요청 : " + newPrice);

            kOrder.Req_Order("반대편선물주문", typePuchase, newPrice);
        }

        public void Order_SellRight(SectionInfo info_)
        {
            kOrder.CleanAll();

            kOrder.PrintLog("매도권 주문 시작 " + info_.min + " ~ " + info_.max + " , 현재가: " + Global.GetStockPrice() + " / 직전가 : " + Global.GetPreStockPrice());
            for (int i = 1; i <= Global.numericUpDown_SellRight_Mount; ++i)
            {
                float orderPrice = i * Global.oneDigit + info_.min;
                if (orderPrice >= info_.max)
                    break;


                kOrder.PrintLog("매도권 주문 신청 : " + orderPrice);
                kOrder.Req_Order("선물매도권_주문", "1", orderPrice);
            }

        }

        SectionInfo GetBuyRightSection()
        {
            float curPrice = Global.GetStockPrice();
            float preCurPrice = Global.GetPreStockPrice();

            List<SectionInfo> listEnable = Global.listSectionInfo.FindAll(x => x.state == 0);
            foreach (SectionInfo item in listEnable)
            {
                if ((item.max + Global.numericUpDown_EnterValue * Global.oneDigit) >= curPrice)
                {
                    if ((item.max + Global.numericUpDown_EnterValue * Global.oneDigit) < preCurPrice)
                    {
                        if ((item.min + Global.numericUpDown_CleanValue * Global.oneDigit) < curPrice)
                            return item;
                    }
                }
            }

            return null;
        }

        public void Order_BuyRight(SectionInfo info_)
        {
            kOrder.CleanAll();

            kOrder.PrintLog("매수권 주문 시작 " + info_.min + " ~ " + info_.max + " , 현재가: " + Global.GetStockPrice() + " / 직전가 : " + Global.GetPreStockPrice());

            for (int i = 1; i <= Global.numericUpDown_SellRight_Mount; ++i)
            {
                float orderPrice = info_.max - i * Global.oneDigit;
                if (orderPrice <= info_.min)
                    break;

                kOrder.PrintLog("매수권 주문 신청 : " + orderPrice);
                kOrder.Req_Order("선물매수권_주문", "2", orderPrice);
                //ReqManager.Request_Order("선물주문", Global.selectedCode, TypeOrder.NewPurchase, TypePuchase.매수, TypePrice.Custom, Global.NumPerDigit, orderPrice.ToString(), "");
            }
        }

        void RefreshSectionUI()
        {
            dataGridView_SectionInfo.Invoke(new MethodInvoker(delegate ()
            {
                dataGridView_SectionInfo.DataSource = null;
                dataGridView_SectionInfo.DataSource = Global.listSectionInfo;
                dataGridView_SectionInfo.Refresh();
            }));
        }

        void DoSectionWork()
        {
            float curPrice = Global.GetStockPrice();
            bool upPrice = false;
            if (curPrice >= Global.GetPreStockPrice())
                upPrice = true;

            List<SectionInfo> listEnable = Global.listSectionInfo;

            if (upPrice)
            {
                //매도 지정가 주문을 위한 조건
                SectionInfo processSellRight = listEnable.Find(x => x.state == 1);
                if (processSellRight == null)
                {
                    SectionInfo targetSellRight = GetSellRightSection();
                    if (targetSellRight != null)
                    {
                        targetSellRight.state = 1;
                        Order_SellRight(targetSellRight);
                        RefreshSectionUI();
                    }
                }
                else
                {
                    if ((processSellRight.max - (Global.numericUpDown_CleanValue * Global.oneDigit)) < curPrice)
                    {
                        kOrder.PrintLog("지정가 매도 구간 청산!");
                        kOrder.CleanAll();
                        processSellRight.state = -1;
                        RefreshSectionUI();
                    }
                }
            }
            else
            {
                //매수 지정가 주문을 위한 조건
                SectionInfo processBuyRight = listEnable.Find(x => x.state == 2);
                if (processBuyRight == null)
                {
                    SectionInfo targetBuyRight = GetBuyRightSection();
                    if (targetBuyRight != null)
                    {
                        targetBuyRight.state = 2;
                        Order_BuyRight(targetBuyRight);
                        RefreshSectionUI();
                    }
                }
                else
                {
                    if ((processBuyRight.min + (Global.numericUpDown_CleanValue * Global.oneDigit)) > curPrice)
                    {
                        kOrder.PrintLog("지정가 매수 구간 청산!");
                        kOrder.CleanAll();
                        processBuyRight.state = -1;
                        RefreshSectionUI();
                    }
                }
            }

        }

        void ThreadWork()
        {
            while(true)
            {
                if (KiwoomAPI.Get.GetConnectState() == 0)
                {
                    ButtonInit();
                    return;
                }
                    

                if (Global.stop)
                    break;

                DoSectionWork();

            }

            kOrder.Req_FutureAccountInfo();
            kOrder.Req_TotalProfit();
            kOrder.Req_HavingFutureStockInfo();
            kOrder.Req_OrderingInfo();

            kOrder.PrintLog("Worker done");

            ButtonInit();
        }

        void ButtonInit()
        {
            this.Invoke(new MethodInvoker(delegate () {
                button_Start.Enabled = true;
                button_Stop.Enabled = false;
                button_CheckAcc.Enabled = true;
                button_FixSection.Enabled = true;

                //dataGridView_SectionInfo.Enabled = true;
                groupBox_SettingInfo.Enabled = true;

            }));
        }

        private void button_Start_Click(object sender, EventArgs e)
        {
            Global.stop = false;

            Thread worker = new Thread(ThreadWork);
            worker.Start();

            button_Start.Enabled = false;
            button_Stop.Enabled = true;
            button_CheckAcc.Enabled = false;
            button_FixSection.Enabled = false;

            //dataGridView_SectionInfo.Enabled = false;
            groupBox_SettingInfo.Enabled = false;
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            Global.stop = true;

            this.Invoke(new MethodInvoker(delegate () {
                button_Stop.Enabled = false;

                Global.logViewer.SaveLog();
            }));
        }
    }
}
