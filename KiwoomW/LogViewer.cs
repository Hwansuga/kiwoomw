﻿using AxKHOpenAPILib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KiwoomW
{
    public partial class LogViewer : Form
    {
        public LogViewer()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | Global.CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        private void LogViewer_Load(object sender, EventArgs e)
        {

        }

        public void Print_CommonLog(string strMsg_)
        {
            listBox_CommonLog.Invoke((MethodInvoker)delegate
            {
                Util.AutoLineStringAdd(listBox_CommonLog, strMsg_);
            });

        }

        public void Print_RealTimeLog(string strMsg_)
        {
            listBox_RealTime.Invoke((MethodInvoker)delegate
            {
                Util.AutoLineStringAdd(listBox_RealTime, strMsg_);
            });
        }

        public void OnEventConnect(object sender, _DKHOpenAPIEvents_OnEventConnectEvent e)
        {
            Print_CommonLog("◀OnEventConnect << nErrCode : " + e.nErrCode);
        }

        public void OnReceiveConditionVer(object sender, _DKHOpenAPIEvents_OnReceiveConditionVerEvent e)
        {
            Print_CommonLog("◀OnReceiveConditionVer << lRet : " + e.lRet + " / sMsg : " + e.sMsg);
        }

        public void OnReceiveTrCondition(object sender, _DKHOpenAPIEvents_OnReceiveTrConditionEvent e)
        {
            Print_CommonLog("◀OnReceiveTrCondition << nIndex : " + e.nIndex + " / strConditionName : " + e.strConditionName + " / sScrNo :" + e.sScrNo);
        }

        public void OnReceiveRealCondition(object sender, _DKHOpenAPIEvents_OnReceiveRealConditionEvent e)
        {
            Print_CommonLog("◀OnReceiveRealCondition << sTrCode : " + e.sTrCode + " / strConditionName : " + e.strConditionName);
        }

        public void OnReceiveTrData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            Print_CommonLog("◀OnReceiveTrData << sErrorCode : " + e.sErrorCode + " / sMessage : " + e.sMessage);
        }

        public void OnReceiveRealData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            Print_RealTimeLog("◀OnReceiveRealData << sRealType : " + e.sRealType + " / sRealKey : " + e.sRealKey + " / sRealData : " + e.sRealData);
        }

        public void OnReceiveChejanData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveChejanDataEvent e)
        {
            Print_CommonLog("◀OnReceiveChejanData << nItemCnt : " + e.nItemCnt + " / sGubun : " + e.sGubun + " / sFIdList : " + e.sFIdList);
            foreach(var item in e.sFIdList.Split(';'))
            {
                string value = KiwoomAPI.Get.GetChejanData(int.Parse(item)).Trim();
                if (string.IsNullOrEmpty(value) == false)
                    Print_CommonLog(item + " : " + value);
            }
        }

        public void OnReceiveMsg(object sender, _DKHOpenAPIEvents_OnReceiveMsgEvent e)
        {
            Print_CommonLog("◀OnReceiveMsg << " + e.sMsg + " / " + e.sRQName + " / " + e.sScrNo + " / " + e.sTrCode);
        }

        public void SaveLog()
        {
            Util.SaveTxtFile(DateTime.Now.ToString("yyyyMMdd_HH-mm_") + "CommonLog.txt", listBox_CommonLog.Items.Cast<string>().ToList());            
        }
    }
}
