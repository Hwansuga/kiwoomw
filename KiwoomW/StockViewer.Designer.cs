﻿namespace KiwoomW
{
    partial class StockViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StockViewer));
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView_HavingStockInfo = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridView_OderInfo = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_HavingStockInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_OderInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 13;
            this.label1.Text = "== 잔고 현황 ==";
            // 
            // dataGridView_HavingStockInfo
            // 
            this.dataGridView_HavingStockInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_HavingStockInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView_HavingStockInfo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView_HavingStockInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_HavingStockInfo.Location = new System.Drawing.Point(13, 25);
            this.dataGridView_HavingStockInfo.Name = "dataGridView_HavingStockInfo";
            this.dataGridView_HavingStockInfo.RowHeadersVisible = false;
            this.dataGridView_HavingStockInfo.RowTemplate.Height = 23;
            this.dataGridView_HavingStockInfo.Size = new System.Drawing.Size(470, 176);
            this.dataGridView_HavingStockInfo.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 204);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "== 주문 현황 ==";
            // 
            // dataGridView_OderInfo
            // 
            this.dataGridView_OderInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_OderInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView_OderInfo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView_OderInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_OderInfo.Location = new System.Drawing.Point(13, 219);
            this.dataGridView_OderInfo.Name = "dataGridView_OderInfo";
            this.dataGridView_OderInfo.RowHeadersVisible = false;
            this.dataGridView_OderInfo.RowTemplate.Height = 23;
            this.dataGridView_OderInfo.Size = new System.Drawing.Size(470, 176);
            this.dataGridView_OderInfo.TabIndex = 16;
            // 
            // StockViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 406);
            this.Controls.Add(this.dataGridView_OderInfo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dataGridView_HavingStockInfo);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StockViewer";
            this.Text = "StockViewer";
            this.Load += new System.EventHandler(this.StockViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_HavingStockInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_OderInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView_HavingStockInfo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridView_OderInfo;
    }
}