﻿namespace KiwoomW
{
    partial class LogViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogViewer));
            this.label3 = new System.Windows.Forms.Label();
            this.listBox_RealTime = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox_CommonLog = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(580, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 12);
            this.label3.TabIndex = 11;
            this.label3.Text = "실시간 로그";
            // 
            // listBox_RealTime
            // 
            this.listBox_RealTime.FormattingEnabled = true;
            this.listBox_RealTime.ItemHeight = 12;
            this.listBox_RealTime.Location = new System.Drawing.Point(580, 33);
            this.listBox_RealTime.Name = "listBox_RealTime";
            this.listBox_RealTime.Size = new System.Drawing.Size(404, 328);
            this.listBox_RealTime.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "일반로그";
            // 
            // listBox_CommonLog
            // 
            this.listBox_CommonLog.FormattingEnabled = true;
            this.listBox_CommonLog.ItemHeight = 12;
            this.listBox_CommonLog.Location = new System.Drawing.Point(12, 33);
            this.listBox_CommonLog.Name = "listBox_CommonLog";
            this.listBox_CommonLog.Size = new System.Drawing.Size(562, 328);
            this.listBox_CommonLog.TabIndex = 6;
            // 
            // LogViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 377);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listBox_RealTime);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox_CommonLog);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LogViewer";
            this.Text = "LogViewer";
            this.Load += new System.EventHandler(this.LogViewer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBox_RealTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox_CommonLog;
    }
}