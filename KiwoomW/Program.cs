﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KiwoomW
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string mutexKey = "{D74B2DBB-35B8-4046-BAEA-531FE315E079}";

            //check duplication
            if (Util.CheckDuplication(mutexKey))
                return;

//             if (Util.CheckExpireDate(new DateTime(2019, 7, 27)))
//             {
//                 MessageBox.Show("기간이 만료되었습니다.");
//                 return;
//             }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
