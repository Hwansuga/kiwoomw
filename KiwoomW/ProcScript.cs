﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

class ProcData
{
    public string procName = "";
    public Func<object[] , object> action;
    public Action endAction;
    public Action failAction;
    public object[] param;

    public object DoProc()
    {
        return action(param);
    }
}

class ProcScript
{
    public List<ProcData> listRealProc = new List<ProcData>();

    public List<ProcData> listProcData = new List<ProcData>();

    public ProcData curJob = null;

    public ProcScript()
    {

    }

    public void AddProc(Func<object[], object> action_ , object[] param_ = null , string name_ = "" , Action endAction_ = null , Action failAction_ = null)
    {
        ProcData procData = new ProcData
        {
            action = action_,
            param = param_,
            procName = name_,
            endAction = endAction_,
            failAction = failAction_
        };
        listProcData.Add(procData);
        NextProc();
    }

    public virtual void StartCurJob()
    {
        object ret = curJob.DoProc();       
    }

    void NextProc()
    {
        if (curJob != null)
            return;

        if (listProcData.Count <= 0)
            return;

        curJob = listProcData[0];
        listProcData.RemoveAt(0);

        StartCurJob();
    }

    public void DoneProc()
    {
        curJob = null;
        NextProc();
    }

    public void StopCurrentProc()
    {
        curJob = null;
        DoneProc();
    }

    public void DeleteProc(string name_)
    {
        listProcData.RemoveAll(x => x.procName == name_);
    }

    public void DeleteAllProc()
    {
        listProcData.Clear();
        StopCurrentProc();
    }
}

