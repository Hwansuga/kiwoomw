﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

class EventUtil : Singleton<EventUtil>
{
    public static void ExitEvent(object sender, FormClosingEventArgs e)
    {
        Application.ExitThread();
        Environment.Exit(0);
    }

    public static void KeyPress_OnlyNumber(object sender, KeyPressEventArgs e)
    {
        if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
        {
            e.Handled = true;
        }
    }

    public static void ConnectCustomEvent(object uiContorller_)
    {
        List<FieldInfo> listField = uiContorller_.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
        List<FieldInfo> listNumericUpDown = listField.FindAll(x => x.FieldType == typeof(NumericUpDown));
        foreach (FieldInfo item in listNumericUpDown)
        {
            NumericUpDown temp = (NumericUpDown)item.GetValue(uiContorller_);
            temp.Click += numericUpDownCustom_ValueChanged;
            temp.Leave += numericUpDownCustom_ValueChanged;
            temp.ValueChanged += numericUpDownCustom_ValueChanged;
        }

        List<FieldInfo> listCombobox = listField.FindAll(x => x.FieldType == typeof(ComboBox));
        foreach (FieldInfo item in listCombobox)
        {
            ComboBox temp = (ComboBox)item.GetValue(uiContorller_);
            temp.SelectedIndexChanged += comboBox_SelectedIndexChanged;
        }

        List<FieldInfo> listRadioButton = listField.FindAll(x => x.FieldType == typeof(RadioButton));
        foreach (FieldInfo item in listRadioButton)
        {
            RadioButton temp = (RadioButton)item.GetValue(uiContorller_);
            temp.Click += radioButtonCustom_CheckedChanged;
        }

        List<FieldInfo> listCheckBox = listField.FindAll(x => x.FieldType == typeof(CheckBox));
        foreach (FieldInfo item in listCheckBox)
        {
            CheckBox temp = (CheckBox)item.GetValue(uiContorller_);
            temp.Click += checkBoxCustom_CheckedChanged;
        }
    }

    public static void numericUpDownCustom_ValueChanged(object sender, EventArgs e)
    {
        NumericUpDown temp = (NumericUpDown)sender;
        Util.SetValue(Global.Instance, temp.Name, temp.Value);
    }

    public static void textBoxCustom_TextChanged(object sender, EventArgs e)
    {
        TextBox temp = (TextBox)sender;
        Util.SetValue(Global.Instance, temp.Name, temp.Text);
    }

    public static void radioButtonCustom_CheckedChanged(object sender, EventArgs e)
    {
        RadioButton temp = (RadioButton)sender;
        Util.SetValue(Global.Instance, temp.Name, temp.Checked);
    }

    public static void checkBoxCustom_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox temp = (CheckBox)sender;
        Util.SetValue(Global.Instance, temp.Name, temp.Checked);
    }

    public static void comboBoxCustom_CheckedChanged(object sender, EventArgs e)
    {
        ComboBox temp = (ComboBox)sender;
        Util.SetValue(Global.Instance, temp.Name, temp.Text);
    }

    public static void labelCustom_TextChanged(object sender, EventArgs e)
    {
        Label temp = (Label)sender;
        Util.SetValue(Global.Instance, temp.Name, temp.Text);
    }

    public static void comboBox_SelectedIndexChanged(object sender, EventArgs e)
    {
        ComboBox temp = (ComboBox)sender;
        Util.SetValue(Global.Instance, temp.Name, temp.SelectedItem);
    }
}

