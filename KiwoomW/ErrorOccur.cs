﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class ErrorOccur
    {
        public static Dictionary<int, string> ErrorCode = new Dictionary<int, string>();

        static ErrorOccur()
        {
            ErrorCode.Add(0, "정상처리");
            ErrorCode.Add(-10, "실패");
            ErrorCode.Add(-100, "사용자정보교환 실패");
            ErrorCode.Add(-101, "서버접속 실패");
            ErrorCode.Add(-102, "버전처리 실패");
            ErrorCode.Add(-103, "개인방화벽 실패");
            ErrorCode.Add(-104, "메모리보호 실패");
            ErrorCode.Add(-105, "함수입력값 오류");
            ErrorCode.Add(-106, "통신연결 종료");
            ErrorCode.Add(-200, "시세조회 과부하");
            ErrorCode.Add(-201, "전문작성초기화 실패");
            ErrorCode.Add(-202, "전문작성입력값 오류");
            ErrorCode.Add(-203, "데이터 없음");
            ErrorCode.Add(-204, "조회가능한 종목수 초과");
            ErrorCode.Add(-205, "데이터수신 실패");
            ErrorCode.Add(-206, "조회가능한 FID수 초과");
            ErrorCode.Add(-207, "실시간 해제오류");
            ErrorCode.Add(-300, "입력값 오류");
            ErrorCode.Add(-301, "계좌비밀번호 없음");
            ErrorCode.Add(-302, "타인계좌사용 오류");
            ErrorCode.Add(-303, "주문가격이 20억원을 초과");
            ErrorCode.Add(-304, "주문가격이 50억원을 초과");
            ErrorCode.Add(-305, "주문수량이 총발행주수의 1 % 초과 오류");
            ErrorCode.Add(-306, "주문수량은 총발행주수의 3 % 초과 오류");
            ErrorCode.Add(-307, "주문전송 실패");
            ErrorCode.Add(-308, "주문전송 과부하");
            ErrorCode.Add(-309, "주문수량 300계약초과");
            ErrorCode.Add(-310, "주문수량 500계약초과");
            ErrorCode.Add(-340, "계좌정보없음");
            ErrorCode.Add(-500, "종목코드없음");
        }

        public static string GetErrString(int code_)
        {
            if (ErrorCode.ContainsKey(code_))
                return ErrorCode[code_];
            else
                return "알수없는 오류";
        }
    }

