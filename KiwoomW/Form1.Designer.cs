﻿namespace KiwoomW
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label_Server = new System.Windows.Forms.Label();
            this.label_UserID = new System.Windows.Forms.Label();
            this.label_UserName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label_KospiPrice = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox_KospiCode = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_Account = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label_curTime = new System.Windows.Forms.Label();
            this.label_winLoseValue = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label_winLoseValue2 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label_winLoseValue3 = new System.Windows.Forms.Label();
            this.button_CheckAcc = new System.Windows.Forms.Button();
            this.button_Allclear = new System.Windows.Forms.Button();
            this.button_Stop = new System.Windows.Forms.Button();
            this.button_Start = new System.Windows.Forms.Button();
            this.listBox_ListProc = new System.Windows.Forms.ListBox();
            this.groupBox_SettingInfo = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDown_MaxPriceSection = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_MinPriceSection = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numericUpDown_Level_SellRight = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.numericUpDown_NumPerDigit = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDown_SellRight_Mount = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.numericUpDown_EnterValue = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDown_Term_BuyRight = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDown_remainSec = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_CleanValue = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.dataGridView_SectionInfo = new System.Windows.Forms.DataGridView();
            this.dataGridView_AccountInfo = new System.Windows.Forms.DataGridView();
            this.button_FixSection = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox_SettingInfo.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxPriceSection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MinPriceSection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Level_SellRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NumPerDigit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SellRight_Mount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_EnterValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Term_BuyRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_remainSec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CleanValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_SectionInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_AccountInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1056, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.loginToolStripMenuItem.Text = "Login";
            this.loginToolStripMenuItem.Click += new System.EventHandler(this.loginToolStripMenuItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label_Server, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label_UserID, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label_UserName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 27);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(270, 86);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // label_Server
            // 
            this.label_Server.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Server.AutoSize = true;
            this.label_Server.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_Server.Location = new System.Drawing.Point(138, 56);
            this.label_Server.Name = "label_Server";
            this.label_Server.Size = new System.Drawing.Size(129, 30);
            this.label_Server.TabIndex = 9;
            this.label_Server.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_UserID
            // 
            this.label_UserID.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_UserID.AutoSize = true;
            this.label_UserID.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_UserID.Location = new System.Drawing.Point(138, 28);
            this.label_UserID.Name = "label_UserID";
            this.label_UserID.Size = new System.Drawing.Size(129, 28);
            this.label_UserID.TabIndex = 8;
            this.label_UserID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_UserName
            // 
            this.label_UserName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_UserName.AutoSize = true;
            this.label_UserName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_UserName.Location = new System.Drawing.Point(138, 0);
            this.label_UserName.Name = "label_UserName";
            this.label_UserName.Size = new System.Drawing.Size(129, 28);
            this.label_UserName.TabIndex = 7;
            this.label_UserName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Info;
            this.label3.Location = new System.Drawing.Point(3, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 28);
            this.label3.TabIndex = 2;
            this.label3.Text = "아이디";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.Info;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 28);
            this.label2.TabIndex = 1;
            this.label2.Text = "사용자이름";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Info;
            this.label5.Location = new System.Drawing.Point(3, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 30);
            this.label5.TabIndex = 4;
            this.label5.Text = "접속서버";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label_KospiPrice, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.comboBox_KospiCode, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 119);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(270, 58);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // label_KospiPrice
            // 
            this.label_KospiPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_KospiPrice.AutoSize = true;
            this.label_KospiPrice.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_KospiPrice.Location = new System.Drawing.Point(138, 29);
            this.label_KospiPrice.Name = "label_KospiPrice";
            this.label_KospiPrice.Size = new System.Drawing.Size(129, 29);
            this.label_KospiPrice.TabIndex = 14;
            this.label_KospiPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(3, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 29);
            this.label7.TabIndex = 13;
            this.label7.Text = "현재가";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 29);
            this.label6.TabIndex = 11;
            this.label6.Text = "KOSPI200 종목";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBox_KospiCode
            // 
            this.comboBox_KospiCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_KospiCode.BackColor = System.Drawing.SystemColors.Window;
            this.comboBox_KospiCode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_KospiCode.FormattingEnabled = true;
            this.comboBox_KospiCode.Location = new System.Drawing.Point(138, 3);
            this.comboBox_KospiCode.Name = "comboBox_KospiCode";
            this.comboBox_KospiCode.Size = new System.Drawing.Size(129, 20);
            this.comboBox_KospiCode.TabIndex = 12;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.label1, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.comboBox_Account, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label17, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label_curTime, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.label_winLoseValue, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.label18, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label_winLoseValue2, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.label19, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.label_winLoseValue3, 1, 4);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(12, 183);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 5;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(270, 118);
            this.tableLayoutPanel4.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Info;
            this.label1.Location = new System.Drawing.Point(3, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 26);
            this.label1.TabIndex = 19;
            this.label1.Text = "순자산금액";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Info;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "계좌번호";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBox_Account
            // 
            this.comboBox_Account.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_Account.BackColor = System.Drawing.SystemColors.Window;
            this.comboBox_Account.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_Account.FormattingEnabled = true;
            this.comboBox_Account.Location = new System.Drawing.Point(138, 3);
            this.comboBox_Account.Name = "comboBox_Account";
            this.comboBox_Account.Size = new System.Drawing.Size(129, 20);
            this.comboBox_Account.TabIndex = 5;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.SystemColors.Info;
            this.label17.Location = new System.Drawing.Point(3, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(129, 23);
            this.label17.TabIndex = 6;
            this.label17.Text = "현재시간";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_curTime
            // 
            this.label_curTime.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_curTime.AutoSize = true;
            this.label_curTime.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_curTime.Location = new System.Drawing.Point(138, 23);
            this.label_curTime.Name = "label_curTime";
            this.label_curTime.Size = new System.Drawing.Size(129, 23);
            this.label_curTime.TabIndex = 15;
            this.label_curTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_winLoseValue
            // 
            this.label_winLoseValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_winLoseValue.AutoSize = true;
            this.label_winLoseValue.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_winLoseValue.Location = new System.Drawing.Point(138, 46);
            this.label_winLoseValue.Name = "label_winLoseValue";
            this.label_winLoseValue.Size = new System.Drawing.Size(129, 23);
            this.label_winLoseValue.TabIndex = 16;
            this.label_winLoseValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.SystemColors.Info;
            this.label18.Location = new System.Drawing.Point(3, 46);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(129, 23);
            this.label18.TabIndex = 7;
            this.label18.Text = "당일매도손익";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_winLoseValue2
            // 
            this.label_winLoseValue2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_winLoseValue2.AutoSize = true;
            this.label_winLoseValue2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_winLoseValue2.Location = new System.Drawing.Point(138, 69);
            this.label_winLoseValue2.Name = "label_winLoseValue2";
            this.label_winLoseValue2.Size = new System.Drawing.Size(129, 23);
            this.label_winLoseValue2.TabIndex = 18;
            this.label_winLoseValue2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.SystemColors.Info;
            this.label19.Location = new System.Drawing.Point(3, 69);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(129, 23);
            this.label19.TabIndex = 17;
            this.label19.Text = "선물정산차금";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_winLoseValue3
            // 
            this.label_winLoseValue3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_winLoseValue3.AutoSize = true;
            this.label_winLoseValue3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_winLoseValue3.Location = new System.Drawing.Point(138, 92);
            this.label_winLoseValue3.Name = "label_winLoseValue3";
            this.label_winLoseValue3.Size = new System.Drawing.Size(129, 26);
            this.label_winLoseValue3.TabIndex = 20;
            this.label_winLoseValue3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_CheckAcc
            // 
            this.button_CheckAcc.BackColor = System.Drawing.Color.GhostWhite;
            this.button_CheckAcc.Enabled = false;
            this.button_CheckAcc.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button_CheckAcc.Location = new System.Drawing.Point(12, 307);
            this.button_CheckAcc.Name = "button_CheckAcc";
            this.button_CheckAcc.Size = new System.Drawing.Size(270, 25);
            this.button_CheckAcc.TabIndex = 13;
            this.button_CheckAcc.Text = "계좌조회";
            this.button_CheckAcc.UseVisualStyleBackColor = false;
            this.button_CheckAcc.Click += new System.EventHandler(this.button_CheckAcc_Click);
            // 
            // button_Allclear
            // 
            this.button_Allclear.Location = new System.Drawing.Point(12, 338);
            this.button_Allclear.Name = "button_Allclear";
            this.button_Allclear.Size = new System.Drawing.Size(132, 23);
            this.button_Allclear.TabIndex = 26;
            this.button_Allclear.Text = "청산";
            this.button_Allclear.UseVisualStyleBackColor = true;
            this.button_Allclear.Click += new System.EventHandler(this.button_Allclear_Click);
            // 
            // button_Stop
            // 
            this.button_Stop.BackColor = System.Drawing.Color.GhostWhite;
            this.button_Stop.Enabled = false;
            this.button_Stop.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button_Stop.Location = new System.Drawing.Point(81, 367);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(63, 23);
            this.button_Stop.TabIndex = 29;
            this.button_Stop.Text = "Stop";
            this.button_Stop.UseVisualStyleBackColor = false;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // button_Start
            // 
            this.button_Start.BackColor = System.Drawing.Color.GhostWhite;
            this.button_Start.Enabled = false;
            this.button_Start.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button_Start.Location = new System.Drawing.Point(12, 367);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(63, 23);
            this.button_Start.TabIndex = 28;
            this.button_Start.Text = "Start";
            this.button_Start.UseVisualStyleBackColor = false;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // listBox_ListProc
            // 
            this.listBox_ListProc.FormattingEnabled = true;
            this.listBox_ListProc.ItemHeight = 12;
            this.listBox_ListProc.Location = new System.Drawing.Point(285, 218);
            this.listBox_ListProc.Name = "listBox_ListProc";
            this.listBox_ListProc.Size = new System.Drawing.Size(222, 172);
            this.listBox_ListProc.TabIndex = 30;
            // 
            // groupBox_SettingInfo
            // 
            this.groupBox_SettingInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_SettingInfo.AutoSize = true;
            this.groupBox_SettingInfo.Controls.Add(this.tableLayoutPanel3);
            this.groupBox_SettingInfo.Enabled = false;
            this.groupBox_SettingInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox_SettingInfo.Location = new System.Drawing.Point(513, 26);
            this.groupBox_SettingInfo.Name = "groupBox_SettingInfo";
            this.groupBox_SettingInfo.Size = new System.Drawing.Size(271, 264);
            this.groupBox_SettingInfo.TabIndex = 31;
            this.groupBox_SettingInfo.TabStop = false;
            this.groupBox_SettingInfo.Text = "설정 정보";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_MaxPriceSection, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_MinPriceSection, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label14, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label13, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label9, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_Level_SellRight, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label15, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_NumPerDigit, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.label11, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_SellRight_Mount, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.label16, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_EnterValue, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_Term_BuyRight, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_remainSec, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_CleanValue, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.label20, 0, 8);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 15);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 9;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(261, 229);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // numericUpDown_MaxPriceSection
            // 
            this.numericUpDown_MaxPriceSection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_MaxPriceSection.Location = new System.Drawing.Point(188, 28);
            this.numericUpDown_MaxPriceSection.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_MaxPriceSection.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_MaxPriceSection.Name = "numericUpDown_MaxPriceSection";
            this.numericUpDown_MaxPriceSection.Size = new System.Drawing.Size(70, 21);
            this.numericUpDown_MaxPriceSection.TabIndex = 34;
            this.numericUpDown_MaxPriceSection.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_MaxPriceSection.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numericUpDown_MinPriceSection
            // 
            this.numericUpDown_MinPriceSection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_MinPriceSection.Location = new System.Drawing.Point(188, 3);
            this.numericUpDown_MinPriceSection.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_MinPriceSection.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_MinPriceSection.Name = "numericUpDown_MinPriceSection";
            this.numericUpDown_MinPriceSection.Size = new System.Drawing.Size(70, 21);
            this.numericUpDown_MinPriceSection.TabIndex = 33;
            this.numericUpDown_MinPriceSection.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_MinPriceSection.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.Info;
            this.label14.Location = new System.Drawing.Point(3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(179, 25);
            this.label14.TabIndex = 10;
            this.label14.Text = "하한선";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.Info;
            this.label13.Location = new System.Drawing.Point(3, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(179, 25);
            this.label13.TabIndex = 9;
            this.label13.Text = "상한선";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.Info;
            this.label9.Location = new System.Drawing.Point(3, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(179, 25);
            this.label9.TabIndex = 2;
            this.label9.Text = "구간설정(틱단위)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_Level_SellRight
            // 
            this.numericUpDown_Level_SellRight.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_Level_SellRight.Location = new System.Drawing.Point(188, 53);
            this.numericUpDown_Level_SellRight.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_Level_SellRight.Name = "numericUpDown_Level_SellRight";
            this.numericUpDown_Level_SellRight.Size = new System.Drawing.Size(70, 21);
            this.numericUpDown_Level_SellRight.TabIndex = 0;
            this.numericUpDown_Level_SellRight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_Level_SellRight.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.Info;
            this.label15.Location = new System.Drawing.Point(3, 125);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(179, 25);
            this.label15.TabIndex = 15;
            this.label15.Text = "지정가구매수량";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_NumPerDigit
            // 
            this.numericUpDown_NumPerDigit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_NumPerDigit.Location = new System.Drawing.Point(188, 128);
            this.numericUpDown_NumPerDigit.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_NumPerDigit.Name = "numericUpDown_NumPerDigit";
            this.numericUpDown_NumPerDigit.Size = new System.Drawing.Size(70, 21);
            this.numericUpDown_NumPerDigit.TabIndex = 16;
            this.numericUpDown_NumPerDigit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_NumPerDigit.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.Info;
            this.label11.Location = new System.Drawing.Point(3, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(179, 25);
            this.label11.TabIndex = 5;
            this.label11.Text = "지정가 단계";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_SellRight_Mount
            // 
            this.numericUpDown_SellRight_Mount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_SellRight_Mount.Location = new System.Drawing.Point(188, 103);
            this.numericUpDown_SellRight_Mount.Name = "numericUpDown_SellRight_Mount";
            this.numericUpDown_SellRight_Mount.Size = new System.Drawing.Size(70, 21);
            this.numericUpDown_SellRight_Mount.TabIndex = 6;
            this.numericUpDown_SellRight_Mount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.SystemColors.Info;
            this.label16.Location = new System.Drawing.Point(3, 75);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(179, 25);
            this.label16.TabIndex = 17;
            this.label16.Text = "진입설정";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_EnterValue
            // 
            this.numericUpDown_EnterValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_EnterValue.Location = new System.Drawing.Point(188, 78);
            this.numericUpDown_EnterValue.Name = "numericUpDown_EnterValue";
            this.numericUpDown_EnterValue.Size = new System.Drawing.Size(70, 21);
            this.numericUpDown_EnterValue.TabIndex = 18;
            this.numericUpDown_EnterValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.Info;
            this.label10.Location = new System.Drawing.Point(3, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(179, 25);
            this.label10.TabIndex = 3;
            this.label10.Text = "익절";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_Term_BuyRight
            // 
            this.numericUpDown_Term_BuyRight.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_Term_BuyRight.Location = new System.Drawing.Point(188, 153);
            this.numericUpDown_Term_BuyRight.Name = "numericUpDown_Term_BuyRight";
            this.numericUpDown_Term_BuyRight.Size = new System.Drawing.Size(70, 21);
            this.numericUpDown_Term_BuyRight.TabIndex = 4;
            this.numericUpDown_Term_BuyRight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.Info;
            this.label12.Location = new System.Drawing.Point(3, 175);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(179, 25);
            this.label12.TabIndex = 23;
            this.label12.Text = "제한시간(초)";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label12.Visible = false;
            // 
            // numericUpDown_remainSec
            // 
            this.numericUpDown_remainSec.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_remainSec.Location = new System.Drawing.Point(188, 178);
            this.numericUpDown_remainSec.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDown_remainSec.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDown_remainSec.Name = "numericUpDown_remainSec";
            this.numericUpDown_remainSec.Size = new System.Drawing.Size(70, 21);
            this.numericUpDown_remainSec.TabIndex = 24;
            this.numericUpDown_remainSec.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_remainSec.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDown_remainSec.Visible = false;
            // 
            // numericUpDown_CleanValue
            // 
            this.numericUpDown_CleanValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_CleanValue.Location = new System.Drawing.Point(188, 203);
            this.numericUpDown_CleanValue.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDown_CleanValue.Name = "numericUpDown_CleanValue";
            this.numericUpDown_CleanValue.Size = new System.Drawing.Size(70, 21);
            this.numericUpDown_CleanValue.TabIndex = 14;
            this.numericUpDown_CleanValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_CleanValue.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.SystemColors.Info;
            this.label20.Location = new System.Drawing.Point(3, 200);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(179, 29);
            this.label20.TabIndex = 25;
            this.label20.Text = "손절";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView_SectionInfo
            // 
            this.dataGridView_SectionInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView_SectionInfo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView_SectionInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_SectionInfo.Location = new System.Drawing.Point(790, 27);
            this.dataGridView_SectionInfo.Name = "dataGridView_SectionInfo";
            this.dataGridView_SectionInfo.RowHeadersVisible = false;
            this.dataGridView_SectionInfo.RowTemplate.Height = 23;
            this.dataGridView_SectionInfo.Size = new System.Drawing.Size(258, 363);
            this.dataGridView_SectionInfo.TabIndex = 32;
            // 
            // dataGridView_AccountInfo
            // 
            this.dataGridView_AccountInfo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_AccountInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_AccountInfo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView_AccountInfo.Location = new System.Drawing.Point(285, 26);
            this.dataGridView_AccountInfo.Name = "dataGridView_AccountInfo";
            this.dataGridView_AccountInfo.RowHeadersVisible = false;
            this.dataGridView_AccountInfo.RowTemplate.Height = 23;
            this.dataGridView_AccountInfo.Size = new System.Drawing.Size(222, 189);
            this.dataGridView_AccountInfo.TabIndex = 33;
            // 
            // button_FixSection
            // 
            this.button_FixSection.Location = new System.Drawing.Point(513, 296);
            this.button_FixSection.Name = "button_FixSection";
            this.button_FixSection.Size = new System.Drawing.Size(271, 23);
            this.button_FixSection.TabIndex = 34;
            this.button_FixSection.Text = "구간값 재 적용";
            this.button_FixSection.UseVisualStyleBackColor = true;
            this.button_FixSection.Click += new System.EventHandler(this.button_FixSection_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 404);
            this.Controls.Add(this.button_FixSection);
            this.Controls.Add(this.dataGridView_AccountInfo);
            this.Controls.Add(this.dataGridView_SectionInfo);
            this.Controls.Add(this.groupBox_SettingInfo);
            this.Controls.Add(this.listBox_ListProc);
            this.Controls.Add(this.button_Stop);
            this.Controls.Add(this.button_Start);
            this.Controls.Add(this.button_Allclear);
            this.Controls.Add(this.button_CheckAcc);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBox_SettingInfo.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxPriceSection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MinPriceSection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Level_SellRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NumPerDigit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SellRight_Mount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_EnterValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Term_BuyRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_remainSec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CleanValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_SectionInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_AccountInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label_Server;
        private System.Windows.Forms.Label label_UserID;
        private System.Windows.Forms.Label label_UserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label_KospiPrice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox_KospiCode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_Account;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label_curTime;
        private System.Windows.Forms.Label label_winLoseValue;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label_winLoseValue2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button_CheckAcc;
        private System.Windows.Forms.Button button_Allclear;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.ListBox listBox_ListProc;
        private System.Windows.Forms.GroupBox groupBox_SettingInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericUpDown_Level_SellRight;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numericUpDown_NumPerDigit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numericUpDown_SellRight_Mount;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numericUpDown_EnterValue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDown_Term_BuyRight;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericUpDown_remainSec;
        private System.Windows.Forms.NumericUpDown numericUpDown_CleanValue;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridView dataGridView_SectionInfo;
        private System.Windows.Forms.NumericUpDown numericUpDown_MinPriceSection;
        private System.Windows.Forms.NumericUpDown numericUpDown_MaxPriceSection;
        private System.Windows.Forms.DataGridView dataGridView_AccountInfo;
        private System.Windows.Forms.Button button_FixSection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_winLoseValue3;
    }
}

