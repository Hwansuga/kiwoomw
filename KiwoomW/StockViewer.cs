﻿using AxKHOpenAPILib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KiwoomW
{
    public partial class StockViewer : Form
    {
        public StockViewer()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | Global.CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        public void OnReceiveTrData(object sender, _DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            MethodInfo func = this.GetType().GetMethod(e.sRQName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (func == null)
                return;

            object[] prams = new object[1];
            prams[0] = e;
            func.Invoke(this, prams);
        }

        private void StockViewer_Load(object sender, EventArgs e)
        {

        }

        void Ans_HavingFutureStockInfo(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            int cnt = KiwoomAPI.Get.GetRepeatCnt(e.sTrCode, e.sRQName);

            Global.listStockInfo.Clear();
            for (int i = 0; i < cnt; ++i)
            {
                if (int.Parse(KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, i, "보유수량").Trim()) <= 0)
                {
                    continue;
                }

                StockInfo stockInfo = new StockInfo();

                List<PropertyInfo> listField = stockInfo.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
                foreach (var item in listField)
                {
                    string value = KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, i, item.Name).Trim();
                    item.SetValue(stockInfo, value);
                }

                Global.listStockInfo.Add(stockInfo);
            }

            dataGridView_HavingStockInfo.Invoke(new MethodInvoker(delegate () {
                dataGridView_HavingStockInfo.DataSource = null;
                dataGridView_HavingStockInfo.DataSource = Global.listStockInfo;
                dataGridView_HavingStockInfo.Refresh();
            }));
        }

        void Ans_OrderingInfo(_DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {            
            int cnt = KiwoomAPI.Get.GetRepeatCnt(e.sTrCode, e.sRQName);

            Global.listOrderIfo.Clear();
            for (int i = 0; i < cnt; ++i)
            {
                OrderInfo stockInfo = new OrderInfo();

                List<PropertyInfo> listField = stockInfo.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
                foreach (var item in listField)
                {
                    string value = KiwoomAPI.Get.GetCommData(e.sTrCode, e.sRQName, i, item.Name).Trim();
                    item.SetValue(stockInfo, value);
                }
                Global.listOrderIfo.Add(stockInfo);
            }

            dataGridView_OderInfo.Invoke(new MethodInvoker(delegate () {
                dataGridView_OderInfo.DataSource = null;
                dataGridView_OderInfo.DataSource = Global.listOrderIfo;
                dataGridView_OderInfo.Refresh();
            }));
        }
    }
}
