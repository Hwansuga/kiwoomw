﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


class CallbackEventUtil : Singleton<CallbackEventUtil>
{
    public static void AttachCallbackEvent(object instanc_, string strNameEvent_ , List<object> listEventReceiver_)
    {
        foreach (object receiver in listEventReceiver_)
            AttachCallbackEvent(instanc_ , strNameEvent_ , receiver);            
    }

    public static void AttachCallbackEvent(object instanc_, string strNameEvent_, object receiver_)
    {
        MethodInfo func = receiver_.GetType().GetMethod(strNameEvent_, BindingFlags.Public | BindingFlags.Instance);
        if (func == null)
            return;

        EventInfo eventInfo = instanc_.GetType().GetEvent(strNameEvent_);
        Type type = eventInfo.EventHandlerType;
        Delegate action = Delegate.CreateDelegate(type, receiver_, func);

        eventInfo.AddEventHandler(instanc_, action);
    }
}

