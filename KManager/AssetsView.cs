﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UtilProject.Model;

namespace KManager
{
    public partial class AssetsView : UserControl
    {
        public AssetsView()
        {
            InitializeComponent();
        }

        private void AssetsView_Load(object sender, EventArgs e)
        {

        }

        public void UpdateData(List<MStock> _source)
        {
            label_Deposit.Text = Global.deposit.ToString("n0");

            var parsOption = NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign;

            label_TotalPurchaseAmount.Text = _source.Sum(x => long.Parse(x.PurchaseAmount, parsOption)).ToString("N0");
            label_TotalValuation.Text = _source.Sum(x => long.Parse(x.Evaluation, parsOption)).ToString("N0");

            var totalProfitLoss = _source.Sum(x => long.Parse(x.ProfitLoss, parsOption));
            label_TotalReturn.ForeColor = (totalProfitLoss >= 0) ? Color.Red : Color.Blue;
            label_TotalReturn.Text = totalProfitLoss.ToString("N0");

            var totalRate = Math.Round((double)long.Parse(label_TotalReturn.Text, parsOption) / long.Parse(label_TotalPurchaseAmount.Text, parsOption) * 100, 2);
            label_TotalProfitLoss.ForeColor = (totalRate >= 0) ? Color.Red : Color.Blue;
            label_TotalProfitLoss.Text = $"{totalRate}%";

            //키움 : 추정자산에서 매도비용은 수수료 미포함
            label_Assets.Text = (
                Global.deposit
                + long.Parse(label_TotalPurchaseAmount.Text, parsOption)
                + totalProfitLoss
                + _source.Sum(x => long.Parse(x.BuyCharge, parsOption))
                ).ToString("N0");
        }
    }
}
