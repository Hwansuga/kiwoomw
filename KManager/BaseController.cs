﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class BaseController
{
    protected static Dictionary<string, int> screenNoDic = new Dictionary<string, int>();

    public static float charge = 0.0015f;
    public static float tex = 0.0018f;

    public static Action<string, Color?> logSystemEvent = null;
    public static Action<string, Color?> logRealEvent = null;
    public static Action<string, Color?> logConditionEvent = null;
    public static Action<string, Color?> logCheckEvent = null;
    public static Action<string, Color?> logExcutionEvent = null;
    protected static int GetScreenNo(string screenName_)
    {
        if (screenNoDic.ContainsKey(screenName_) == false)
        {
            screenNoDic.Add(screenName_, 5000 + screenNoDic.Count);
        }

        return screenNoDic[screenName_];
    }
    protected static void PrintLog(string _msg)
    {
        logSystemEvent?.Invoke($"{DateTime.Now.ToString("MMdd_HH:mm:ss")}>>{_msg}", Color.Black);
    }

    protected static void PrintRealDataLog(string _msg)
    {
        logRealEvent?.Invoke($"{DateTime.Now.ToString("MMdd_HH:mm:ss")}>>{_msg}", Color.Black);
    }

    protected static void PrintConditionDataLog(string _msg)
    {
        logConditionEvent?.Invoke($"{DateTime.Now.ToString("MMdd_HH:mm:ss")}>>{_msg}", Color.Black);
    }

    protected static void PrintCheckDataLog(string _msg)
    {
        logCheckEvent?.Invoke($"{DateTime.Now.ToString("MMdd_HH:mm:ss")}>>{_msg}", Color.Black);
    }

    protected static void PrintExcutionDataLog(string _msg)
    {
        logExcutionEvent?.Invoke($"{DateTime.Now.ToString("MMdd_HH:mm:ss")}>>{_msg}", Color.Black);
    }
}

