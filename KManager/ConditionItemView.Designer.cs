﻿
namespace KManager
{
    partial class ConditionItemView
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_Condition = new System.Windows.Forms.ListBox();
            this.button_LoadCondition = new System.Windows.Forms.Button();
            this.button_RegCondition = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox_Condition
            // 
            this.listBox_Condition.Dock = System.Windows.Forms.DockStyle.Top;
            this.listBox_Condition.FormattingEnabled = true;
            this.listBox_Condition.ItemHeight = 15;
            this.listBox_Condition.Location = new System.Drawing.Point(0, 0);
            this.listBox_Condition.Name = "listBox_Condition";
            this.listBox_Condition.Size = new System.Drawing.Size(180, 94);
            this.listBox_Condition.TabIndex = 0;
            // 
            // button_LoadCondition
            // 
            this.button_LoadCondition.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_LoadCondition.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_LoadCondition.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button_LoadCondition.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.button_LoadCondition.Location = new System.Drawing.Point(0, 94);
            this.button_LoadCondition.Name = "button_LoadCondition";
            this.button_LoadCondition.Size = new System.Drawing.Size(180, 30);
            this.button_LoadCondition.TabIndex = 2;
            this.button_LoadCondition.Text = "Load Condition";
            this.button_LoadCondition.UseVisualStyleBackColor = true;
            this.button_LoadCondition.Click += new System.EventHandler(this.button_LoadCondition_Click);
            // 
            // button_RegCondition
            // 
            this.button_RegCondition.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_RegCondition.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_RegCondition.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button_RegCondition.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.button_RegCondition.Location = new System.Drawing.Point(0, 124);
            this.button_RegCondition.Name = "button_RegCondition";
            this.button_RegCondition.Size = new System.Drawing.Size(180, 30);
            this.button_RegCondition.TabIndex = 3;
            this.button_RegCondition.Text = "RegCondition";
            this.button_RegCondition.UseVisualStyleBackColor = true;
            this.button_RegCondition.Click += new System.EventHandler(this.button_RegCondition_Click);
            // 
            // ConditionItemView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button_RegCondition);
            this.Controls.Add(this.button_LoadCondition);
            this.Controls.Add(this.listBox_Condition);
            this.Name = "ConditionItemView";
            this.Size = new System.Drawing.Size(180, 154);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_Condition;
        private System.Windows.Forms.Button button_LoadCondition;
        private System.Windows.Forms.Button button_RegCondition;
    }
}
