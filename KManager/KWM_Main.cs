﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KManager
{
    public partial class KWM_Main : Form
    {
        public KWM_Main()
        {
            InitializeComponent();

            KWController.Init(Handle);

            FormClosing += ExitEvent;
        }

        void ExitEvent(object sender, FormClosingEventArgs e)
        {
            KWController.Clear();

            Application.ExitThread();
            Environment.Exit(0);
        }

        void Init()
        {
            Global.logSystemEvent = new ConsoleTextBoxWriter(richTextBox_LogSystem);
            Global.logRealEvent = new ConsoleTextBoxWriter(richTextBox_Real);
            Global.logConditionEvent = new ConsoleTextBoxWriter(richTextBox_Condition);
            Global.logExcutionEvent = new ConsoleTextBoxWriter(richTextBox_Execution);
            Global.logCheckEvent = new ConsoleTextBoxWriter(richTextBox_Check);

            KWController.logSystemEvent = Global.PrintSystemLog;
            KWController.logRealEvent = Global.PrintRealLog;
            KWController.logConditionEvent = Global.PrintConditionLog;
            KWController.logExcutionEvent = Global.PrintExcutionLog;
            KWController.logCheckEvent = Global.PrintCheckLog;
        }

        private void KWM_Main_Load(object sender, EventArgs e)
        {
            Init();
        }

        private void button_Login_Click(object sender, EventArgs e)
        {
            button_Login.Enabled = false;
            KWController.Connect((_s, _e) => {
                if (_e.nErrCode == 0)
                {
                    Global.PrintSystemLog("success connection!!", Color.Blue);

                    label_Name.Text = KWController.GetLoginInfo("USER_NAME");
                    label_ID.Text = KWController.GetLoginInfo("USER_ID");

                    KWController.GetLoginInfo("ACCOUNT_CNT");
                    KWController.GetLoginInfo("KEY_BSECGB");
                    KWController.GetLoginInfo("FIREW_SECGB");

                    label_SType.Text = string.Empty;

                    comboBox_Acc.DataSource = KWController.GetLoginInfo("ACCNO").Trim().Split(';').ToList().FindAll(x => string.IsNullOrEmpty(x) == false);
                    if (comboBox_Acc.Items.Count > 0)
                        comboBox_Acc.SelectedIndex = 0;

                }
                else
                {
                    button_Login.Enabled = true;
                    Global.PrintSystemLog("failed connection!!", Color.Red);
                    label_Name.Text = string.Empty;
                    label_ID.Text = string.Empty;
                    label_SType.Text = string.Empty;
                }
            });
        }

        private void button_CheckAcc_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(comboBox_Acc.SelectedItem.ToString().Trim()))
            {
                MessageBox.Show("Please select your account!");
                return;
            }

            //Event_CheckAcc();
        }
    }
}
