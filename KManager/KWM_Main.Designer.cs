﻿
namespace KManager
{
    partial class KWM_Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button_Login = new System.Windows.Forms.Button();
            this.richTextBox_LogSystem = new System.Windows.Forms.RichTextBox();
            this.richTextBox_Real = new System.Windows.Forms.RichTextBox();
            this.tabControl_Log = new System.Windows.Forms.TabControl();
            this.SystemLog = new System.Windows.Forms.TabPage();
            this.SLog = new System.Windows.Forms.TabPage();
            this.Condition = new System.Windows.Forms.TabPage();
            this.richTextBox_Condition = new System.Windows.Forms.RichTextBox();
            this.Execution = new System.Windows.Forms.TabPage();
            this.richTextBox_Execution = new System.Windows.Forms.RichTextBox();
            this.Check = new System.Windows.Forms.TabPage();
            this.richTextBox_Check = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.conditionItemView1 = new KManager.ConditionItemView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.assetsView1 = new KManager.AssetsView();
            this.comboBox_Acc = new System.Windows.Forms.ComboBox();
            this.button_CheckAcc = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label_SType = new System.Windows.Forms.Label();
            this.label_ID = new System.Windows.Forms.Label();
            this.label_Name = new System.Windows.Forms.Label();
            this.tabControl_StockAcc = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView_Balance = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridView_NotConcluded = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage_Condition = new System.Windows.Forms.TabPage();
            this.dataGridView_Condition = new System.Windows.Forms.DataGridView();
            this.tabPage_Search = new System.Windows.Forms.TabPage();
            this.dataGridView_Search = new System.Windows.Forms.DataGridView();
            this.tabControl_Log.SuspendLayout();
            this.SystemLog.SuspendLayout();
            this.SLog.SuspendLayout();
            this.Condition.SuspendLayout();
            this.Execution.SuspendLayout();
            this.Check.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabControl_StockAcc.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Balance)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_NotConcluded)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage_Condition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Condition)).BeginInit();
            this.tabPage_Search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Search)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Login
            // 
            this.button_Login.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_Login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Login.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button_Login.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.button_Login.Location = new System.Drawing.Point(3, 111);
            this.button_Login.Name = "button_Login";
            this.button_Login.Size = new System.Drawing.Size(180, 31);
            this.button_Login.TabIndex = 0;
            this.button_Login.Text = "Login";
            this.button_Login.UseVisualStyleBackColor = true;
            this.button_Login.Click += new System.EventHandler(this.button_Login_Click);
            // 
            // richTextBox_LogSystem
            // 
            this.richTextBox_LogSystem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_LogSystem.Location = new System.Drawing.Point(3, 3);
            this.richTextBox_LogSystem.Name = "richTextBox_LogSystem";
            this.richTextBox_LogSystem.Size = new System.Drawing.Size(1070, 199);
            this.richTextBox_LogSystem.TabIndex = 0;
            this.richTextBox_LogSystem.Text = "";
            // 
            // richTextBox_Real
            // 
            this.richTextBox_Real.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_Real.Location = new System.Drawing.Point(3, 3);
            this.richTextBox_Real.Name = "richTextBox_Real";
            this.richTextBox_Real.Size = new System.Drawing.Size(1070, 199);
            this.richTextBox_Real.TabIndex = 1;
            this.richTextBox_Real.Text = "";
            // 
            // tabControl_Log
            // 
            this.tabControl_Log.Controls.Add(this.SystemLog);
            this.tabControl_Log.Controls.Add(this.SLog);
            this.tabControl_Log.Controls.Add(this.Condition);
            this.tabControl_Log.Controls.Add(this.Execution);
            this.tabControl_Log.Controls.Add(this.Check);
            this.tabControl_Log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl_Log.Location = new System.Drawing.Point(0, 536);
            this.tabControl_Log.Name = "tabControl_Log";
            this.tabControl_Log.SelectedIndex = 0;
            this.tabControl_Log.Size = new System.Drawing.Size(1084, 233);
            this.tabControl_Log.TabIndex = 2;
            // 
            // SystemLog
            // 
            this.SystemLog.Controls.Add(this.richTextBox_LogSystem);
            this.SystemLog.Location = new System.Drawing.Point(4, 24);
            this.SystemLog.Name = "SystemLog";
            this.SystemLog.Padding = new System.Windows.Forms.Padding(3);
            this.SystemLog.Size = new System.Drawing.Size(1076, 205);
            this.SystemLog.TabIndex = 0;
            this.SystemLog.Text = "SystemLog";
            this.SystemLog.UseVisualStyleBackColor = true;
            // 
            // SLog
            // 
            this.SLog.Controls.Add(this.richTextBox_Real);
            this.SLog.Location = new System.Drawing.Point(4, 24);
            this.SLog.Name = "SLog";
            this.SLog.Padding = new System.Windows.Forms.Padding(3);
            this.SLog.Size = new System.Drawing.Size(1076, 205);
            this.SLog.TabIndex = 1;
            this.SLog.Text = "SLog";
            this.SLog.UseVisualStyleBackColor = true;
            // 
            // Condition
            // 
            this.Condition.Controls.Add(this.richTextBox_Condition);
            this.Condition.Location = new System.Drawing.Point(4, 24);
            this.Condition.Name = "Condition";
            this.Condition.Size = new System.Drawing.Size(1076, 205);
            this.Condition.TabIndex = 2;
            this.Condition.Text = "Condition";
            this.Condition.UseVisualStyleBackColor = true;
            // 
            // richTextBox_Condition
            // 
            this.richTextBox_Condition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_Condition.Location = new System.Drawing.Point(0, 0);
            this.richTextBox_Condition.Name = "richTextBox_Condition";
            this.richTextBox_Condition.Size = new System.Drawing.Size(1076, 205);
            this.richTextBox_Condition.TabIndex = 1;
            this.richTextBox_Condition.Text = "";
            // 
            // Execution
            // 
            this.Execution.Controls.Add(this.richTextBox_Execution);
            this.Execution.Location = new System.Drawing.Point(4, 24);
            this.Execution.Name = "Execution";
            this.Execution.Size = new System.Drawing.Size(1076, 205);
            this.Execution.TabIndex = 3;
            this.Execution.Text = "Execution";
            this.Execution.UseVisualStyleBackColor = true;
            // 
            // richTextBox_Execution
            // 
            this.richTextBox_Execution.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_Execution.Location = new System.Drawing.Point(0, 0);
            this.richTextBox_Execution.Name = "richTextBox_Execution";
            this.richTextBox_Execution.Size = new System.Drawing.Size(1076, 205);
            this.richTextBox_Execution.TabIndex = 2;
            this.richTextBox_Execution.Text = "";
            // 
            // Check
            // 
            this.Check.Controls.Add(this.richTextBox_Check);
            this.Check.Location = new System.Drawing.Point(4, 24);
            this.Check.Name = "Check";
            this.Check.Size = new System.Drawing.Size(1076, 205);
            this.Check.TabIndex = 4;
            this.Check.Text = "Check";
            this.Check.UseVisualStyleBackColor = true;
            // 
            // richTextBox_Check
            // 
            this.richTextBox_Check.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_Check.Location = new System.Drawing.Point(0, 0);
            this.richTextBox_Check.Name = "richTextBox_Check";
            this.richTextBox_Check.Size = new System.Drawing.Size(1076, 205);
            this.richTextBox_Check.TabIndex = 3;
            this.richTextBox_Check.Text = "";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.panel1.Controls.Add(this.conditionItemView1);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(186, 536);
            this.panel1.TabIndex = 3;
            // 
            // conditionItemView1
            // 
            this.conditionItemView1.Location = new System.Drawing.Point(4, 362);
            this.conditionItemView1.Name = "conditionItemView1";
            this.conditionItemView1.Size = new System.Drawing.Size(180, 154);
            this.conditionItemView1.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.assetsView1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.comboBox_Acc, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.button_CheckAcc, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 145);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.5514F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 69.62617F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.35514F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(186, 214);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // assetsView1
            // 
            this.assetsView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.assetsView1.Location = new System.Drawing.Point(3, 32);
            this.assetsView1.Name = "assetsView1";
            this.assetsView1.Size = new System.Drawing.Size(180, 143);
            this.assetsView1.TabIndex = 5;
            // 
            // comboBox_Acc
            // 
            this.comboBox_Acc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.comboBox_Acc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox_Acc.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.comboBox_Acc.FormattingEnabled = true;
            this.comboBox_Acc.Location = new System.Drawing.Point(3, 3);
            this.comboBox_Acc.Name = "comboBox_Acc";
            this.comboBox_Acc.Size = new System.Drawing.Size(180, 23);
            this.comboBox_Acc.TabIndex = 2;
            // 
            // button_CheckAcc
            // 
            this.button_CheckAcc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_CheckAcc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_CheckAcc.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button_CheckAcc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.button_CheckAcc.Location = new System.Drawing.Point(3, 181);
            this.button_CheckAcc.Name = "button_CheckAcc";
            this.button_CheckAcc.Size = new System.Drawing.Size(180, 30);
            this.button_CheckAcc.TabIndex = 1;
            this.button_CheckAcc.Text = "Check Account";
            this.button_CheckAcc.UseVisualStyleBackColor = true;
            this.button_CheckAcc.Click += new System.EventHandler(this.button_CheckAcc_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.button_Login, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label_SType, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label_ID, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label_Name, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(186, 145);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label_SType
            // 
            this.label_SType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_SType.AutoSize = true;
            this.label_SType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label_SType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(161)))), ((int)(((byte)(176)))));
            this.label_SType.Location = new System.Drawing.Point(3, 72);
            this.label_SType.Name = "label_SType";
            this.label_SType.Size = new System.Drawing.Size(180, 36);
            this.label_SType.TabIndex = 2;
            this.label_SType.Text = "SType";
            this.label_SType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_ID
            // 
            this.label_ID.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_ID.AutoSize = true;
            this.label_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label_ID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(161)))), ((int)(((byte)(176)))));
            this.label_ID.Location = new System.Drawing.Point(3, 36);
            this.label_ID.Name = "label_ID";
            this.label_ID.Size = new System.Drawing.Size(180, 36);
            this.label_ID.TabIndex = 1;
            this.label_ID.Text = "ID";
            this.label_ID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_Name
            // 
            this.label_Name.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Name.AutoSize = true;
            this.label_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label_Name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(161)))), ((int)(((byte)(176)))));
            this.label_Name.Location = new System.Drawing.Point(3, 0);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(180, 36);
            this.label_Name.TabIndex = 0;
            this.label_Name.Text = "Name";
            this.label_Name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl_StockAcc
            // 
            this.tabControl_StockAcc.Controls.Add(this.tabPage3);
            this.tabControl_StockAcc.Controls.Add(this.tabPage4);
            this.tabControl_StockAcc.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl_StockAcc.Location = new System.Drawing.Point(186, 0);
            this.tabControl_StockAcc.Name = "tabControl_StockAcc";
            this.tabControl_StockAcc.SelectedIndex = 0;
            this.tabControl_StockAcc.Size = new System.Drawing.Size(898, 246);
            this.tabControl_StockAcc.TabIndex = 4;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView_Balance);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(890, 218);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Balance";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView_Balance
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView_Balance.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_Balance.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.dataGridView_Balance.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_Balance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_Balance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_Balance.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_Balance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_Balance.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_Balance.Name = "dataGridView_Balance";
            this.dataGridView_Balance.RowHeadersVisible = false;
            this.dataGridView_Balance.RowTemplate.Height = 25;
            this.dataGridView_Balance.Size = new System.Drawing.Size(884, 212);
            this.dataGridView_Balance.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridView_NotConcluded);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(890, 218);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Not Concluded";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridView_NotConcluded
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView_NotConcluded.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView_NotConcluded.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.dataGridView_NotConcluded.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_NotConcluded.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_NotConcluded.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView_NotConcluded.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_NotConcluded.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_NotConcluded.Name = "dataGridView_NotConcluded";
            this.dataGridView_NotConcluded.RowHeadersVisible = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView_NotConcluded.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView_NotConcluded.RowTemplate.Height = 25;
            this.dataGridView_NotConcluded.Size = new System.Drawing.Size(884, 212);
            this.dataGridView_NotConcluded.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage_Condition);
            this.tabControl1.Controls.Add(this.tabPage_Search);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(186, 246);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(898, 287);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage_Condition
            // 
            this.tabPage_Condition.Controls.Add(this.dataGridView_Condition);
            this.tabPage_Condition.Location = new System.Drawing.Point(4, 24);
            this.tabPage_Condition.Name = "tabPage_Condition";
            this.tabPage_Condition.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Condition.Size = new System.Drawing.Size(890, 259);
            this.tabPage_Condition.TabIndex = 0;
            this.tabPage_Condition.Text = "Condition";
            this.tabPage_Condition.UseVisualStyleBackColor = true;
            // 
            // dataGridView_Condition
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView_Condition.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView_Condition.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.dataGridView_Condition.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_Condition.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView_Condition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_Condition.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView_Condition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_Condition.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_Condition.Name = "dataGridView_Condition";
            this.dataGridView_Condition.RowHeadersVisible = false;
            this.dataGridView_Condition.RowTemplate.Height = 25;
            this.dataGridView_Condition.Size = new System.Drawing.Size(884, 253);
            this.dataGridView_Condition.TabIndex = 0;
            // 
            // tabPage_Search
            // 
            this.tabPage_Search.Controls.Add(this.dataGridView_Search);
            this.tabPage_Search.Location = new System.Drawing.Point(4, 24);
            this.tabPage_Search.Name = "tabPage_Search";
            this.tabPage_Search.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Search.Size = new System.Drawing.Size(890, 259);
            this.tabPage_Search.TabIndex = 1;
            this.tabPage_Search.Text = "Search";
            this.tabPage_Search.UseVisualStyleBackColor = true;
            // 
            // dataGridView_Search
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView_Search.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView_Search.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.dataGridView_Search.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_Search.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_Search.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView_Search.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_Search.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_Search.Name = "dataGridView_Search";
            this.dataGridView_Search.RowHeadersVisible = false;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView_Search.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView_Search.RowTemplate.Height = 25;
            this.dataGridView_Search.Size = new System.Drawing.Size(884, 253);
            this.dataGridView_Search.TabIndex = 0;
            // 
            // KWM_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(73)))));
            this.ClientSize = new System.Drawing.Size(1084, 769);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.tabControl_StockAcc);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl_Log);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "KWM_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KMW";
            this.Load += new System.EventHandler(this.KWM_Main_Load);
            this.tabControl_Log.ResumeLayout(false);
            this.SystemLog.ResumeLayout(false);
            this.SLog.ResumeLayout(false);
            this.Condition.ResumeLayout(false);
            this.Execution.ResumeLayout(false);
            this.Check.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabControl_StockAcc.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Balance)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_NotConcluded)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage_Condition.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Condition)).EndInit();
            this.tabPage_Search.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Search)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_Login;
        private System.Windows.Forms.RichTextBox richTextBox_LogSystem;
        private System.Windows.Forms.RichTextBox richTextBox_Real;
        private System.Windows.Forms.TabControl tabControl_Log;
        private System.Windows.Forms.TabPage SystemLog;
        private System.Windows.Forms.TabPage SLog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.Label label_ID;
        private System.Windows.Forms.Label label_SType;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TabControl tabControl_StockAcc;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView_Balance;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dataGridView_NotConcluded;
        private System.Windows.Forms.TabPage Condition;
        private System.Windows.Forms.TabPage Execution;
        private System.Windows.Forms.TabPage Check;
        private System.Windows.Forms.RichTextBox richTextBox_Condition;
        private System.Windows.Forms.RichTextBox richTextBox_Execution;
        private System.Windows.Forms.RichTextBox richTextBox_Check;
        private ConditionItemView conditionItemView1;
        private AssetsView assetsView1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage_Condition;
        private System.Windows.Forms.DataGridView dataGridView_Condition;
        private System.Windows.Forms.TabPage tabPage_Search;
        private System.Windows.Forms.DataGridView dataGridView_Search;
        private System.Windows.Forms.ComboBox comboBox_Acc;
        private System.Windows.Forms.Button button_CheckAcc;
    }
}

