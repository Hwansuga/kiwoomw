using KHOpenApi.NET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UtilProject.Model;

namespace KManager
{
    public partial class KManager_Main : Form
    {
        public KManager_Main()
        {
            InitializeComponent();
            KController.Init(Handle);

            FormClosing += ExitEvent;
        }

        void ExitEvent(object sender, FormClosingEventArgs e)
        {
            KController.Clear();

            Application.ExitThread();
            Environment.Exit(0);
        }

        void Init()
        {
            Global.logSystemEvent = new ConsoleTextBoxWriter(richTextBox_LogSystem);
            Global.logRealEvent = new ConsoleTextBoxWriter(richTextBox_Real);
            Global.logConditionEvent = new ConsoleTextBoxWriter(richTextBox_Condition);
            Global.logExcutionEvent = new ConsoleTextBoxWriter(richTextBox_Execution);
            Global.logCheckEvent = new ConsoleTextBoxWriter(richTextBox_Check);

            KController.logSystemEvent = Global.PrintSystemLog;
            KController.logRealEvent = Global.PrintRealLog;
            KController.logConditionEvent = Global.PrintConditionLog;
            KController.logExcutionEvent = Global.PrintExcutionLog;
            KController.logCheckEvent = Global.PrintCheckLog;
        }
        private void KManager_Main_Load(object sender, EventArgs e)
        {
            Init();
        }

        private void button_Login_Click(object sender, EventArgs e)
        {
            button_Login.Enabled = false;
            KController.Connect((_s,_e)=> { 
                if (_e.nErrCode == 0)
                {
                    Global.PrintSystemLog("success connection!!", Color.Blue);
                    label_Name.Text = KController.GetLoginInfo("USER_NAME");
                    label_ID.Text = KController.GetLoginInfo("USER_ID");

                    if (KController.GetLoginInfo("GetServerGubun") == "1")
                        label_SType.Text = "모의";
                    else
                        label_SType.Text = $"실제 {KController.GetLoginInfo("GetServerGubun")}";

                    comboBox_Acc.DataSource = KController.GetLoginInfo("ACCLIST").Trim().Split(';').ToList().FindAll(x=> string.IsNullOrEmpty(x) == false);
                    if (comboBox_Acc.Items.Count > 0)
#if DEBUG
                        comboBox_Acc.SelectedIndex = 1;
#else
                        comboBox_Acc.SelectedIndex = 0;
#endif
                    
                }
                else
                {
                    button_Login.Enabled = true;
                    Global.PrintSystemLog("success connection!!", Color.Red);
                    label_Name.Text = string.Empty;
                    label_ID.Text = string.Empty;
                    label_SType.Text = string.Empty;
                }
            });
        }

        public void UpdateBalance(List<MStock> _source)
        {
            //dataGridView_Balance.DataSource = null;
            //dataGridView_Balance.DataSource = _source;

            foreach(DataGridViewRow row in dataGridView_Balance.Rows)
            {
                var profitLoss = row.Cells["ProfitLoss"].Value;
                if (profitLoss.ToString().Contains("-"))
                    row.Cells["ProfitLoss"].Style = new DataGridViewCellStyle { ForeColor = Color.Blue};
                else
                    row.Cells["ProfitLoss"].Style = new DataGridViewCellStyle { ForeColor = Color.Red };
                var profitLossRate = row.Cells["ProfitLossRate"].Value;
                if (profitLossRate.ToString().Contains("-"))
                    row.Cells["ProfitLossRate"].Style = new DataGridViewCellStyle { ForeColor = Color.Blue };
                else
                    row.Cells["ProfitLossRate"].Style = new DataGridViewCellStyle { ForeColor = Color.Red };

            }
             
            dataGridView_Balance.Refresh();

            assetsView1.UpdateData(_source);
        }

        public void UpdateNotConcluded(List<MOrderStock> _source)
        {
            dataGridView_NotConcluded.DataSource = null;
            dataGridView_NotConcluded.DataSource = _source;
            foreach(DataGridViewRow row in dataGridView_NotConcluded.Rows)
            {
                var orderType = row.Cells["OrderType"].Value;
                if (orderType.ToString().Contains("매도"))
                    row.Cells["OrderType"].Style = new DataGridViewCellStyle { ForeColor = Color.Blue };
                else
                    row.Cells["OrderType"].Style = new DataGridViewCellStyle { ForeColor = Color.Red };
            }

            dataGridView_NotConcluded.Refresh();
        }

        async void Event_CheckAcc()
        {
            button_CheckAcc.Enabled = false;


            var retDeposit = await KController.OPW00004(comboBox_Acc.SelectedItem.ToString(),
                new List<string> { "예수금", "D+2추정예수금" });

            Global.deposit = Convert.ToInt64(retDeposit.Item1["D+2추정예수금"]);

            var retCheckBalanceAcc = await KController.opw00018(comboBox_Acc.SelectedItem.ToString(),
                new List<string> { "총매입금액", "총평가금액", "총평가손익금액", "총수익률(%)", "추정예탁자산" });


            Global.listMyStock = retCheckBalanceAcc.Item2;
            dataGridView_Balance.DataSource = Global.listMyStock;
            UpdateBalance(Global.listMyStock);

            var retCheckNotConcluded = await KController.CheckNotConcluded(comboBox_Acc.SelectedItem.ToString());

            Global.listNotConcluded = retCheckNotConcluded;
            UpdateNotConcluded(retCheckNotConcluded);


            KController.RegRealData("RealData", Global.RegRealDataCode(),(s,e)=> {
                Global.PrintSystemLog($"sRealType : {e.sRealType}");
                var func = GetType().GetMethod(e.sRealType); 
                func?.Invoke(this, new object[] { e });
            });

            button_CheckAcc.Enabled = true;
        }
        public void 주식시세(_DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            Global.PrintSystemLog($"※ {System.Reflection.MethodBase.GetCurrentMethod().Name} Call ※");                      
        }

        public void 주식체결(_DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            Global.PrintSystemLog($"※ {System.Reflection.MethodBase.GetCurrentMethod().Name} Call ※");
            var stock = Global.listMyStock.Find(x => x.Code.Contains(e.sRealKey));
            if (stock != null)
            {
                stock.현재가 = KController.GetCommRealData<long>(stock.Code, 10);
                UpdateBalance(Global.listMyStock);
            }
        }

        public void 잔고(_DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            Global.PrintSystemLog($"※ {System.Reflection.MethodBase.GetCurrentMethod().Name} Call ※");
        }

        public void 파생잔고(_DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            Global.PrintSystemLog($"※ {System.Reflection.MethodBase.GetCurrentMethod().Name} Call ※");
        }

        private void button_CheckAcc_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(comboBox_Acc.SelectedItem.ToString().Trim()))
            {
                MessageBox.Show("Please select your account!");
                return;
            }

            Event_CheckAcc();
        }
    }
}
