﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KManager
{
    public partial class ConditionItemView : UserControl
    {
        public ConditionItemView()
        {
            InitializeComponent();
        }

        private void button_LoadCondition_Click(object sender, EventArgs e)
        {
            KController.GetConditionNameList((_ret) => {
                Global.listCondition = _ret;
                listBox_Condition.DataSource = _ret.Select(x=>x.name).ToList();
            });
        }

        private void button_RegCondition_Click(object sender, EventArgs e)
        {
            var info = Global.listCondition.Find(x => x.name == listBox_Condition.GetItemText(listBox_Condition.SelectedItem));
            if (info == null)
                return;

            var ret = KController.RegConditionData("RegConditionData", info, (sender, e) =>
            {
                if (e.strType == "I")
                {

                }
                else if (e.strType == "D")
                {

                }
            });

            if (ret)
            {
                Global.PrintSystemLog("Success RegConditionData");
            }
            else
            {
                Global.PrintSystemLog("Failed RegConditionData");
            }
        }
    }
}
