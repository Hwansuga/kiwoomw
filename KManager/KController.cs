﻿
using KHOpenApi.NET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilProject.Model;

class KController : BaseController
{
    static AxKHOpenAPI axKHOpenAPI;
    
    static int realType = 0;
    static List<string> listRealCode = new List<string>();
  
    static _DKHOpenAPIEvents_OnReceiveRealDataEventHandler _callbackRealData = null;

    static ConditionInfo regCondition = null;
    static _DKHOpenAPIEvents_OnReceiveRealConditionEventHandler _callbackContionData = null;

    static _DKHOpenAPIEvents_OnReceiveChejanDataEventHandler _callbackChejan = null;
    
    public static void Init(IntPtr _handle)
    {
        axKHOpenAPI = new AxKHOpenAPI(_handle);

        axKHOpenAPI.OnReceiveRealData += (sender, e) =>
        {
            PrintRealDataLog($"◀RealData {e.sRealType} >> {Newtonsoft.Json.JsonConvert.SerializeObject(e)}");
            _callbackRealData?.Invoke(sender, e);
        };

        axKHOpenAPI.OnReceiveRealCondition += (sender, e) => {
            PrintConditionDataLog($"◀RealCondition {e.strType} >> {Newtonsoft.Json.JsonConvert.SerializeObject(e)}");
            _callbackContionData?.Invoke(sender, e);
        };

        axKHOpenAPI.OnReceiveChejanData += (sender, e) => {
            PrintConditionDataLog($"◀ChejanData {e.sGubun} >> {Newtonsoft.Json.JsonConvert.SerializeObject(e)}");
            _callbackChejan?.Invoke(sender, e);
        };
    }

    public static void Clear()
    {
        foreach (KeyValuePair<string, int> item in screenNoDic)
            axKHOpenAPI.DisconnectRealData(item.Value.ToString());

        axKHOpenAPI.SetRealRemove("ALL", "ALL");
    }

    public static bool Created()
    {
        return axKHOpenAPI.Created;
    }
    public static void Connect(_DKHOpenAPIEvents_OnEventConnectEventHandler _callback)
    {
        PrintLog($"▶{System.Reflection.MethodBase.GetCurrentMethod().Name}_Req");
        axKHOpenAPI.CommConnect();

        _DKHOpenAPIEvents_OnEventConnectEventHandler action = null;
        action = (sender, e) => {
            PrintLog($"◀{System.Reflection.MethodBase.GetCurrentMethod().Name}_Ans >> {Newtonsoft.Json.JsonConvert.SerializeObject(e)}");
            axKHOpenAPI.OnEventConnect -= action;

            if (e.nErrCode == 0)
            {
                axKHOpenAPI.KOA_Functions("ShowAccountWindow","");

                //모의
                if (GetLoginInfo("GetServerGubun") == "1")
                    charge = 0.0035f;
            }
          
            _callback(sender , e);
        };

        axKHOpenAPI.OnEventConnect += action;
    }

    public static string GetLoginInfo(string _sTag)
    {
        var ret = axKHOpenAPI.GetLoginInfo(_sTag);
        PrintLog($"■ {System.Reflection.MethodBase.GetCurrentMethod().Name} : {_sTag} / {ret}■");
        return ret;       
    }

    public static void TrData(string _rqName, string _trCode, Dictionary<string,string> _params, _DKHOpenAPIEvents_OnReceiveTrDataEventHandler _callback)
    {
        foreach (var item in _params)
            axKHOpenAPI.SetInputValue(item.Key , item.Value);

        PrintCheckDataLog($"▶{_rqName}_Req");
        axKHOpenAPI.CommRqData(_rqName, _trCode, 0, GetScreenNo(_rqName).ToString());

        _DKHOpenAPIEvents_OnReceiveTrDataEventHandler action = null;
        action = (sender, e) => {
            if (e.sRQName != _rqName)
                return;

            PrintCheckDataLog($"◀{_rqName}_Ans >> {Newtonsoft.Json.JsonConvert.SerializeObject(e)}");
            axKHOpenAPI.OnReceiveTrData -= action;

            _callback(sender, e);
        };

        axKHOpenAPI.OnReceiveTrData += action;      
    }

    public static string GetCommData(string _trCode, string _sRQName, int _index, string _itemName)
    {
        var ret = axKHOpenAPI.GetCommData(_trCode, _sRQName, _index, _itemName);
        PrintCheckDataLog($"■ {System.Reflection.MethodBase.GetCurrentMethod().Name} ({_trCode}) : {_itemName} / {ret.Trim()}■");
        return ret.Trim();
    }

    public static void OPW00004(string _acc, List<string> _listGetData, Action<Dictionary<string,string>, List<MStock>> _callback)
    {
        var param = new Dictionary<string, string>() {
                {"계좌번호",_acc.Trim() },
                {"비밀번호",""},
                { "상장폐지조회구분", "0"},
                { "비밀번호입력매체구분", "00"}
            };

        var ret = new Dictionary<string, string>();
        var retStock = new List<MStock>();
        TrData(System.Reflection.MethodBase.GetCurrentMethod().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, param, (_sender, _e) => {
            _listGetData.ForEach(x => ret.Add(x, GetCommData(_e.sTrCode, _e.sRQName, 0, x)));
            var cnt = axKHOpenAPI.GetRepeatCnt(_e.sTrCode, _e.sRQName);
            for (int i=0; i<cnt; ++i)
            {
                var stockInfo = new MStock();
                stockInfo.GetType().GetFields().ToList().ForEach(x => { 
                    if (x.FieldType == typeof(string))
                        x.SetValue(stockInfo, GetCommData(_e.sTrCode, _e.sRQName, i, x.Name));
                    else
                    {
                        var converter = TypeDescriptor.GetConverter(x.FieldType);
                        var targetData = GetCommData(_e.sTrCode, _e.sRQName, i, x.Name);
                        if (string.IsNullOrEmpty(targetData) == false)
                        {
                            var targetValue = converter.ConvertFrom(targetData);
                            x.SetValue(stockInfo, targetValue);
                        }                    
                    }
                });
                retStock.Add(stockInfo);
            }
            _callback(ret, retStock);
        });
    }

    public static async Task<Tuple<Dictionary<string, string>, List<MStock>>> OPW00004(string _acc, List<string> _listGetData)
    {
        return await Task.Run(()=> {
            var t = new TaskCompletionSource<Tuple<Dictionary<string, string>, List<MStock>>>();

            OPW00004(_acc, _listGetData, (_ret, _listStock) => t.TrySetResult(new Tuple<Dictionary<string, string>, List<MStock>>(_ret, _listStock)));
            return t.Task;
        });  
    }
    public static void opw00018(string _acc, List<string> _listGetData, Action<Dictionary<string, string>, List<MStock>> _callback)
    {
        var param = new Dictionary<string, string>() {
                {"계좌번호",_acc.Trim() },
                {"비밀번호",""},
                {"조회구분","1"},
                { "비밀번호입력매체구분", "00"}
            };

        var ret = new Dictionary<string, string>();
        var retStock = new List<MStock>();
        TrData(System.Reflection.MethodBase.GetCurrentMethod().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, param, (_sender, _e) => {
            _listGetData.ForEach(x => ret.Add(x, GetCommData(_e.sTrCode, _e.sRQName, 0, x)));
            var cnt = axKHOpenAPI.GetRepeatCnt(_e.sTrCode, _e.sRQName);
            for (int i = 0; i < cnt; ++i)
            {
                var stockInfo = new MStock();
                stockInfo.GetType().GetFields().ToList().ForEach(x => {
                    if (x.FieldType == typeof(string))
                        x.SetValue(stockInfo, GetCommData(_e.sTrCode, _e.sRQName, i, x.Name));
                    else
                    {
                        var converter = TypeDescriptor.GetConverter(x.FieldType);
                        var targetData = GetCommData(_e.sTrCode, _e.sRQName, i, x.Name);
                        if (string.IsNullOrEmpty(targetData) == false)
                        {
                            var targetValue = converter.ConvertFrom(targetData);
                            x.SetValue(stockInfo, targetValue);
                        }
                    }
                });
                retStock.Add(stockInfo);
            }
            _callback(ret, retStock);
        });
    }

    public static async Task<Tuple<Dictionary<string, string>, List<MStock>>> opw00018(string _acc, List<string> _listGetData)
    {
        return await Task.Run(() => {
            var t = new TaskCompletionSource<Tuple<Dictionary<string, string>, List<MStock>>>();

            opw00018(_acc, _listGetData, (_ret, _listStock) => t.TrySetResult(new Tuple<Dictionary<string, string>, List<MStock>>(_ret, _listStock)));
            return t.Task;
        });
    }

    public static void CheckNotConcluded(string _acc, Action<List<MOrderStock>> _callback)
    {
        //opt10075
        var param = new Dictionary<string, string>() {
                {"계좌번호",_acc.Trim() },
                {"전체종목구분","0"},
                { "매매구분", "0"},
                { "체결구분", "1"},
            };

        var retStock = new List<MOrderStock>();
        TrData(System.Reflection.MethodBase.GetCurrentMethod().Name, "opt10075", param, (_sender, _e) => {
            var cnt = axKHOpenAPI.GetRepeatCnt(_e.sTrCode, _e.sRQName);
            for (int i = 0; i < cnt; ++i)
            {
                var stockInfo = new MOrderStock();
                stockInfo.GetType().GetFields().ToList().ForEach(x => {
                    if (x.FieldType == typeof(string))
                        x.SetValue(stockInfo, GetCommData(_e.sTrCode, _e.sRQName, i, x.Name));
                    else
                    {
                        var converter = TypeDescriptor.GetConverter(x.FieldType);
                        var targetValue = converter.ConvertFrom(GetCommData(_e.sTrCode, _e.sRQName, i, x.Name));
                        x.SetValue(stockInfo, targetValue);
                    }
                });
                retStock.Add(stockInfo);
            }
            _callback(retStock);
        });
    }

    public static async Task<List<MOrderStock>> CheckNotConcluded(string _acc)
    {
        return await Task.Run(() => {
            var t = new TaskCompletionSource<List<MOrderStock>>();

            CheckNotConcluded(_acc, (_ret) => t.TrySetResult(_ret));
            return t.Task;
        });
    }

    public static void RegRealData(string _rqName, List<string> listCode_, _DKHOpenAPIEvents_OnReceiveRealDataEventHandler _callback = null)
    {
        axKHOpenAPI.DisconnectRealData(GetScreenNo(_rqName).ToString());

        var target = listCode_.Except(listRealCode).ToList();
        if (target.Count > 0)
        {
            var strCode = string.Join(';', target);
            PrintRealDataLog($"▶RegRealData {_rqName} / {strCode}");
            axKHOpenAPI.SetRealReg(GetScreenNo(_rqName).ToString(),
                strCode,
                "10;12;16;17;18;9201;9001",
                realType.ToString());

            listCode_.AddRange(target);
            realType = 1;

            _callbackRealData = _callback;         
        }
    }

    public static T GetCommRealData<T>(string _code, int _fid)
    {
        var ret = axKHOpenAPI.GetCommRealData(_code, _fid).Replace("-","");
        PrintRealDataLog($"■ {System.Reflection.MethodBase.GetCurrentMethod().Name} : {_code} / {FIDManager.GetString(_fid)} {ret}■");

        var converter = TypeDescriptor.GetConverter(typeof(T));
        var targetValue = converter.ConvertFrom(ret);

        return (T)targetValue;
    }

    public static void GetConditionNameList(Action<List<ConditionInfo>> _callback)
    {
        PrintLog($"▶GetConditionLoad _Req");
        var retLoad = axKHOpenAPI.GetConditionLoad();

        _DKHOpenAPIEvents_OnReceiveConditionVerEventHandler action = null;

        action = (sender, e) => {
            PrintLog($"▶GetConditionLoad_Ans : {Newtonsoft.Json.JsonConvert.SerializeObject(e)}");

            var list = axKHOpenAPI.GetConditionNameList().Split(';').Where(x=> string.IsNullOrEmpty(x) == false).ToList();
            var retInfo = list.ConvertAll<ConditionInfo>(x => new ConditionInfo { number = x.Split('^')[0].Trim(), name = x.Split('^')[1].Trim() }).ToList();

            _callback(retInfo);

            axKHOpenAPI.OnReceiveConditionVer -= action;
        };

        axKHOpenAPI.OnReceiveConditionVer += action;
    }

    public static bool RegConditionData(string _rqName, ConditionInfo _info, _DKHOpenAPIEvents_OnReceiveRealConditionEventHandler _callback = null)
    {
        if (regCondition != null)
        {
            axKHOpenAPI.SendConditionStop(GetScreenNo(_rqName).ToString(), regCondition.name , int.Parse(regCondition.number.Trim()));
            regCondition = null;
        }

        var ret = axKHOpenAPI.SendCondition(GetScreenNo(_rqName).ToString(), _info.name , int.Parse(_info.number), 1);
        if (ret == 1)
        {
            _callbackContionData = _callback;
            return true;
        }
        else
        {
            return false;
        }
    }
}

