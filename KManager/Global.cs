﻿using KManager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UtilProject.Model;

class Global : Singleton<Global>
{
    public static ConsoleTextBoxWriter logSystemEvent = null;
    public static ConsoleTextBoxWriter logRealEvent = null;
    public static ConsoleTextBoxWriter logConditionEvent = null;
    public static ConsoleTextBoxWriter logCheckEvent = null;
    public static ConsoleTextBoxWriter logExcutionEvent = null;


    public static long deposit = 0;
    public static List<MStock> listMyStock = new List<MStock>();
    public static List<MViewStock> listConditionViewStock = new List<MViewStock>();
    public static List<MViewStock> listSearchViewStock = new List<MViewStock>();

    public static List<MOrderStock> listNotConcluded = new List<MOrderStock>();
    public static List<ConditionInfo> listCondition = new List<ConditionInfo>();


    public static List<string> listTotalRealData = new List<string>();

    public static List<string> RegRealDataCode()
    {
        var listCode = listMyStock.Select(x => x.Code).ToList();
        listCode.AddRange(listNotConcluded.Select(x => x.Code).ToList());
        listCode.AddRange(listConditionViewStock.Select(x => x.Code).ToList());
        listCode.AddRange(listSearchViewStock.Select(x => x.Code).ToList());

        return listCode.Distinct().ToList();
    }

    static void PrintLog(ConsoleTextBoxWriter _writer, string _msg, Color _color, bool _bold = false)
    {
        if (_writer == null)
            return;
        _writer.bold = _bold;
        _writer.WriteLineWithColor(_msg, _color);
        _writer.bold = false;
    }
    public static void PrintSystemLog(string _msg, Color? _color = null)
    {
        PrintLog(logSystemEvent, _msg, _color?? Color.Black);
    }
    public static void PrintRealLog(string _msg, Color? _color = null)
    {
        PrintLog(logRealEvent, _msg, _color ?? Color.Black);
    }
    public static void PrintConditionLog(string _msg, Color? _color = null)
    {
        PrintLog(logConditionEvent, _msg, _color ?? Color.Black);
    }
    public static void PrintCheckLog(string _msg, Color? _color = null)
    {
        PrintLog(logCheckEvent, _msg, _color ?? Color.Black);
    }
    public static void PrintExcutionLog(string _msg, Color? _color = null)
    {
        PrintLog(logExcutionEvent, _msg, _color ?? Color.Black);
    }
}

