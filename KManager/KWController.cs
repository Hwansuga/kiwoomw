﻿using KFOpenApi.NET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilProject.Model;

class KWController : BaseController
{
    static AxKFOpenAPI axKFOpenAPI;

    static _DKFOpenAPIEvents_OnReceiveRealDataEventHandler _callbackRealData = null;
    static _DKFOpenAPIEvents_OnReceiveChejanDataEventHandler _callbackChejan = null;
    public static void Init(IntPtr _handle)
    {
        axKFOpenAPI = new AxKFOpenAPI(_handle);

        axKFOpenAPI.OnReceiveRealData += (sender, e) =>
        {
            PrintRealDataLog($"◀RealData {e.sRealType} >> {Newtonsoft.Json.JsonConvert.SerializeObject(e)}");
            _callbackRealData?.Invoke(sender, e);
        };

        axKFOpenAPI.OnReceiveChejanData += (sender, e) => {
            PrintConditionDataLog($"◀ChejanData {e.sGubun} >> {Newtonsoft.Json.JsonConvert.SerializeObject(e)}");
            _callbackChejan?.Invoke(sender, e);
        };
    }

    public static void Clear()
    {
        foreach (KeyValuePair<string, int> item in screenNoDic)
            axKFOpenAPI.DisconnectRealData(item.Value.ToString());
    }
    public static bool Created()
    {
        return axKFOpenAPI.Created;
    }

    public static void Connect(_DKFOpenAPIEvents_OnEventConnectEventHandler _callback)
    {
        PrintLog($"▶{System.Reflection.MethodBase.GetCurrentMethod().Name}_Req");
        axKFOpenAPI.CommConnect(1);

        _DKFOpenAPIEvents_OnEventConnectEventHandler action = null;
        action = (sender, e) => {
            PrintLog($"◀{System.Reflection.MethodBase.GetCurrentMethod().Name}_Ans >> {Newtonsoft.Json.JsonConvert.SerializeObject(e)}");
            axKFOpenAPI.OnEventConnect -= action;

            if (e.nErrCode == 0)
            {
                //모의
                if (GetLoginInfo("GetServerGubun") == "1")
                    charge = 0.0035f;
            }
         
            _callback(sender, e);
        };

        axKFOpenAPI.OnEventConnect += action;
    }

    public static string GetLoginInfo(string _sTag)
    {
        var ret = axKFOpenAPI.GetLoginInfo(_sTag);
        PrintLog($"■ {System.Reflection.MethodBase.GetCurrentMethod().Name} : {_sTag} / {ret}■");
        return ret;
    }
}

