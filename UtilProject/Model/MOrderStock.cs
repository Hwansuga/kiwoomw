﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilProject.Model
{
    public class MOrderStock
    {
        public string 종목코드 = string.Empty;
        public string 종목명 = string.Empty;
        public string 주문번호 = string.Empty;
        public string 주문상태 = string.Empty;
        public long 주문수량 = 0;
        public long 주문가격 = 0;
        public string 주문구분 = string.Empty;
        public long 미체결수량 = 0;
        public long 체결량 = 0;

        
        public string Name { get { return 종목명; } }
        public string OrderID { get { return 주문번호; } }
        public string OrderState { get { return 주문상태; } }
        public string Count { get { return 주문수량.ToString("n0"); } }
        public string Price { get { return 주문가격.ToString("n0"); } }
        public string OrderType { get { return 주문구분.Replace("+","").Replace("-",""); } }
        public string Unconclusion { get { return 미체결수량.ToString(); } }
        public string Conclusion { get { return 체결량.ToString(); } }

        public string Code { get { return 종목코드; } }
    }
}
