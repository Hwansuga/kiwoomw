﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace UtilProject.Model
{
    public class MStock
    {
        public string 종목번호 = string.Empty;
        public string 종목명 = string.Empty;
        public long 매입가 = 0;
        public long 보유수량 = 0;   
        public long 현재가 = 0;
        
        public string Name { get { return 종목명; } }
        public string Count { get { return 보유수량.ToString("n0"); } }
        public string AveragePrice { get { return 매입가.ToString("n0"); } }
        public string CurrentPrice { get { return 현재가.ToString("n0"); } }
        public string PurchaseAmount { get { return (매입가 * 보유수량).ToString("n0"); } }
        public string Evaluation { get { return (현재가* 보유수량).ToString("n0"); } }
        public string ProfitLoss { get { return (
                    ((현재가- 매입가) * 보유수량)
                    - (long.Parse(BuyCharge, NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign) 
                    + long.Parse(SellCharge, NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign)
                    + long.Parse(Tex, NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign)
                    )).ToString("n0"); } }
        public string ProfitLossRate { get { return $"{Math.Round( (double)long.Parse(ProfitLoss, NumberStyles.AllowThousands| NumberStyles.AllowLeadingSign) / long.Parse(PurchaseAmount, NumberStyles.AllowThousands) * 100, 2)}%"; } }

        public string Code { get { return 종목번호; } }

        public string BuyCharge { get { return ((long)((매입가 * 보유수량) * KController.charge / 10) * 10).ToString("n0"); } }
        public string SellCharge { get { return ((long)((현재가 * 보유수량) * KController.charge / 10) * 10).ToString("n0"); } }
        public string Tex { get { return Math.Truncate(현재가 * 보유수량 * KController.tex).ToString("n0"); } }
    }
}
