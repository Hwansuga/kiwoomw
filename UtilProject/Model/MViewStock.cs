﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilProject.Model
{
    class MViewStock
    {
        public string 종목코드 = string.Empty;
        public string 종목명 = string.Empty;
        public long 현재가 = 0;
        public double 전일대비 = 0;
        public double 등락율 = 0;
        public long 거래량 = 0;
        public long 시가 = 0;
        public long 고가 = 0;
        public long 저가 = 0;

        public string Code { get { return 종목코드; } }
        public string Name { get { return 종목명; } }
        public string CurrentPrice { get { return 현재가.ToString("n0"); } }


    }
}
