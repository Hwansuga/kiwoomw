﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class FIDManager : Singleton<FIDManager>
{
    public static Dictionary<int, string> dicData = new Dictionary<int, string>();

    static FIDManager()
    {
        dicData.Add(10, "현재가");
        dicData.Add(27, "(최우선)매도호가");
        dicData.Add(28, "(최우선)매수호가");
        dicData.Add(302, "종목명");
        dicData.Add(900, "주문수량");
        dicData.Add(901, "주문가격");
        dicData.Add(902, "미체결수량");
        dicData.Add(903, "체결누계금액");
        dicData.Add(904, "원주문번호");
        dicData.Add(905, "주문구분");
        dicData.Add(906, "매매구분");
        dicData.Add(907, "매도수구분");
        dicData.Add(908, "주문체결시간");
        dicData.Add(909, "체결번호");
        dicData.Add(910, "체결가");
        dicData.Add(911, "체결량");
        dicData.Add(912, "주문업무분류");
        dicData.Add(913, "주문상태");
        dicData.Add(914, "단위체결가");
        dicData.Add(915, "단위체결량");
        dicData.Add(919, "거부사유");
        dicData.Add(920, "화면번호");
        dicData.Add(921, "터미널번호");
        dicData.Add(922, "신용구분(실시간체결용)");
        dicData.Add(923, "대출일(실시간체결용)");
        dicData.Add(930, "보유수량");
        dicData.Add(931, "매입단가");
        dicData.Add(932, "총매입가(당일누적)");
        dicData.Add(938, "당일매매수수료");
        dicData.Add(939, "당일매매세금");
        dicData.Add(946, "매도/매수구분");
        dicData.Add(950, "당일총매도손익");
        dicData.Add(8019, "손익율(실현손익)");
        dicData.Add(9001, "종목코드");
        dicData.Add(9201, "계좌번호");
        dicData.Add(9203, "주문번호");
        dicData.Add(9205, "관리자사번");
        
    }

    public static string GetString(int code_)
    {
        if (dicData.ContainsKey(code_))
            return dicData[code_];
        else
            return code_.ToString();
    }
}

