﻿using System;

public class Singleton<T> where T : class, new()
{
    public static T Instance
    {
        get;
        private set;
    }

    static Singleton()
    {
        if (Singleton<T>.Instance == null)
        {
            Singleton<T>.Instance = new T();
            (Instance as Singleton<T>).OnCreate();
        }
    }

    public virtual void Clear()
    {
        Singleton<T>.Instance = null;
        Singleton<T>.Instance = new T();
    }

    public virtual void OnCreate()
    {

    }

    public void Raise()
    {

    }
}